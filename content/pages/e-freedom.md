Title: e-Freedom 
Subtitle: Free the Software, Free the World
Date: 2021-03-30
save_as: pages/e-freedom.html
header_cover: images/slackware.png

## Slackware

I, Herag Anarchivist, highly recommend [Slackware](http://slackware.com) as the *One True Linux*. Not just because I'm a [SubGenius](http://subgenius.com). Not just because it is *my* distribution of choice. It is about maximizing _freedom_ and minimizing the necessity of troubleshooting. Slackware my seem old to some, but for those of you that just want your computer to work and do what you want it to do? Slackware is the ultimate choice. 

You don't have to worry about the FOSS politics with Slackware. You don't have to wonder if your operating system is truly free or if there are bits and bytes of non-free code in your system. Slackware simply works. There are very few systems out there that Slackware does't work well on. Some newer systems won't be supported until the upcoming release of Slackware 15, but almost all systems are currently supported. 

Slackware also has a huge community support forum at [Linux Questions](https://linuxquestions.org/questions/slackware-14/) where you can ask just about anything without fear of ridicule. Of course, make sure to search the forums first for the answer. Chances are, your question has already been answered. So be sure to check that out.

But let's install it on your computer now.

#### Minimum System Requirements

- 486 Processor
- 64MB RAM (1GB+ Suggested)
- About 5GB+ of Hard Disk Space for a Full Installation
- CD or DVD Drive or you can use a Bootable USB Drive as well

#### Get Slack

The preferred method for getting Slack is via Torrent.

*Slackware 14.2 x86 [32-bit]*

[Slackware ISO](http://www.slackware.com/torrents/slackware-14.2-install-dvd.torrent)

*Slackware 14.2 x86_64 [64-bit]*

[Slackware64 ISO](https://www.slackware.com/torrents/slackware64-14.2-install-dvd.torrent)

You can also get an ISO from a standard download mirror if you'd prefer.

[Slackware ISO Images](http://mirrors.slackware.com/slackware/slackware-iso/)

#### Burn the Image

We are assuming you are using Windows, so to get that ISO on your USB drive, you will need a tool to do so.

[Rufus](https://rufus.ie)

Once that is installed, it is pretty straight-forward. Write the ISO to your spare USB drive, then you'll need to reboot your system.

#### BIOS Stuff

You'll need to look up how to get to your system BIOS so that you can easily boot to the USB instead of your regular hard drive. This may take some looking around, but on most systems, F12 or Del will usually get you there. Then you'll need to figure out how to boot from your USB drive. It is usually in a section of the BIOS called 'Boot' or 'Boot Options'.

When you figure that out, boot the system into the Slackware Install Drive and it is time to get started.

#### Installation

Now installing is fairly simple. The first step may be a little challenging for some new users, but I'll walk you through it.

When the system is booted, you will see that you are in a command line interface. *Read the instructions.* Always read the instructions. That is key. You'll first want to format your main hard drive. I like to use what is called a TUI (Tangible User Interface). It is not quite graphical, but gives you the feeling of a graphical user interface (GUI). So, I alway use *cfdisk* to format my hard drive. Let's assume your hard drive is identified as *sda* by your new OS.

Type: 

`cfdisk /dev/sda`

It will pull up a TUI interface for that hard drive. You can delete all the partitions and create some new ones. You'll want a swap partition and a main partition. If your system uses UEFI, you'll want a UEFI partition as well. So I always start by creating the swap partition. It can basically be whatever you want. A general rule of thumb is that it should be twice the size of RAM. But if you have a relatively modern computer, it doesn't need to be that for various reasons that we won't go into. I generally stick to the rule that it should be at least 2GB. I made mine 4GB even though I have 16GB of RAM. It's really up to you.

Now, if you have a UEFI system (most modern systems are), you'll want to create a UEFI partition. It only needs to be 100MB. Then you can partition the rest of your drive as the Linux Filesystem.

Next select *write* from the menu and let it write it all to the drive. 

Then you can quit the *cfdisk* tool and type `setup` into the prompt that it drops you in. 

At this point, it is all fairly simple. Just select the first option and follow the steps. Make sure you read. That is very important.

#### LILO and ELILO

Near the end of the installation, you will be confronted with a prompt to install LILO(Linux Loader). This is critical. If you have a UEFI system, you will want to skip LILO and install ELILO. If you don't have a UEFI system, doing the automatic install of LILO should be fine. But it is important that you know what kind of system you have so that you choose the correct loader. After you complete this step, you can rest easy and reboot your computer. Pull that USB drive out that you used for install and boot into your new Linux system! That's it!

#### Afterword

At this point I will direct you to a podcast where Klaatu, the beloved host, goes through every package in Slackware to explain what each does and why they are useful. Check it out! And learn about your new Linux computer!

[GNU World Order](https://gnuworldorder.info)

You can also [email](mailto:anarchobookclub@riseup.net) me with questions, or hit me up on [Mastodon](https://dobbs.town/@herag)
