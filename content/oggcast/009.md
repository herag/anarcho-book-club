Title: Discussion
Date: 2021-01-29 09:55
Modified: 2021-03-10 21:47
Category: discussion
slug: 009
authors: Herag Anarchivist
summary: In which we discuss archetypes and worldviews.
header_cover: images/anarchy.jpg
header_avatar: images/abc-256.png

Here we discuss archetypes and worldviews, personal mythologies and heaven/hell. Also, I urge my dear listener to join me in reading *Player Piano* by Kurt Vonnegut. Find the book at [Anarcho Book Club Library](https://anarchobook.club/library.html). You can then yell at me on [Mastodon](https://dobbs.town/@herag).

Into/Outro music is from Kai Engel's Chapter Four - Fall album. You can get that album and many more of his albums from his Bandcamp [page](https://kaiengel.bandcamp.com/).

<audio controls><source src="oggs/009-discussion.ogg" type="audio/ogg"></audio>
