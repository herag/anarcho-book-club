Title: The Hero With A Thousand Faces 
Subtitle: by Joseph Campbell
Date: 2021-05-31
save_as: pages/campbell.html
header_cover: images/docs.jpg

*These HTML texts have been hand curated by the author of this website. If you find any mistakes, feel free to message him on [Mastodon](https://dobbs.town/@herag)*

[Prologue](https://anarchobook.club/01-campbell.html)

[Chapter One](https://anarchobook.club/02-campbell.html)

[Chapter Two](https://anarchobook.club/03-campbell.html)
