Title: Initiation
Date: 2021-05-21
slug: 03-campbell
save_as: 03-campbell.html
header_cover: images/departure.jpg

## i. The Road of Trials

Once having traversed the threshold, the hero moves in a dream landscape of curiously fluid, ambiguous forms, where he must survive a succession of trials. This is a favorite phase of the mythadventure. It has produced a world literature of miraculous tests and ordeals. The hero is covertly aided by the advice, amulets, and secret agents of the supernatural helper whom he met before his entrance into this region. Or it may be that he here discovers for the first time that there is a benign power everywhere supporting him in his superhuman passage.

One of the best known and most charming examples of the "difficult tasks" motif is that of Psyche?s quest for her lost lover, Cupid . Here all the principal roles are reversed: instead of the lover trying to win his bride, it is the bride trying to win her lover; and instead of a cruel father withholding his daughter from the lover, it is the jealous mother, Venus, hiding her son, Cupid, from his bride. When Psyche pleaded with Venus, the goddess grasped her violently by the hair and dashed her head upon the ground, then took a great quantity of wheat, barley, millet, poppy seed, peas, lentils, and beans, mingled these all together in a heap, and commanded the girl to sort them before night. Psyche was aided by an army of ants. Venus told her,next, to gather the golden wool of certain dangerous wild sheep, sharp of horn and poisonous of bite, that inhabited an inaccessible valley in a dangerous wood. But a green reed instructed her how to gather from the reeds round about the golden locks shed by the sheep in their passage. The goddess now required a bottle of water from a freezing spring high on a towering rock beset by sleepless dragons. An eagle approached, and accomplished the marvelous task. Psyche was ordered, finally, to bring from the abyss of the underworld a box full of supernatural beauty. But a high tower told her how to go down to the world below, gave her coins for Charon and sops for Cerberus, and sped her on her way.

Psyche's voyage to the underworld is but one of innumerable such adventures undertaken by the heroes of fairy tale and myth. Among the most perilous are those of the shamans of the peoples of the farthest north (the Lapps, Siberians, Eskimo, and certain American Indian tribes), when they go to seek out and recover the lost or abducted souls of the sick. The shaman of the Siberians is clothed for the adventure in a magical costume representing a bird or reindeer, the shadow principle of the shaman himself, the shape of his soul. His drum is his animal -- his eagle, reindeer, or horse; he is said to fly or ride on it. The stick that he carries is another of his aids. And he is attended by a host of invisible familiars.

An early voyager among the Lapps has left a vivid description of the weird performance of one of these strange emissaries into the kingdoms of the dead . Since the yonder world is a place of everlasting night, the ceremonial of the shaman has to take place after dark. The friends and neighbors gather in the flickering, dimly lighted hut of the patient, and follow attentively the gesticulations of the magician. First he summons the helping spirits; these arrive, invisible to all but himself. Two women in ceremonial attire, but without belts and wearing linen hoods, a man without hood or belt, and a girl not as yet adult, are in attendance.

The shaman uncovers his head, loosens his belt and shoestrings, covers his face with his hands and begins to twirl in a variety of circles. Suddenly, with very violent gestures, he shouts: "Fit out the reindeer! Ready to boat!" Snatching up an ax, he begins striking himself about the knees with it and swinging it in the direction of the three women. He drags burning logs out of the fire with his naked hands. He dashes three times around each of the women and finally collapses, "like a dead man." During the whole time, no one has been permitted to touch him. While he reposes now in trance, he is to be watched so closely that not even a fly may settle upon him. His spirit has departed, and he is viewing the sacred mountains with their inhabiting gods. The women in attendance whisper to each other, trying to guess in what part of the yonder world he now may be. 

---

The women may be unable to locate the shaman?s position in the yonder world, in which case his spirit may fail to return to the body. Or the wandering spirit of an enemy shaman may engage him in battle or else lead him astray. It is said that there have been many shamans who failed to return.

---

If they mention the correct mountain, the shaman stirs either a hand or a foot. At length he begins to return. In a low, weak voice he utters the words he has heard in the world below. Then the women begin to sing. The shaman slowly awakes, declaring both the cause of the illness and the manner of sacrifice to be made. Then he announces the length of time it will take for the patient to grow well.

"On his laborious journey," reports another observer, 

> the shaman has to encounter and master a number of differing obstacles (pudak) which are not always easily overcome. After he has wandered through dark forests and over massive ranges of mountains, where he occasionally comes across the bones of other shamans and their animal mounts who have died along the way, he reaches an opening in the ground. The most difficult stages of the adventure now begin, when the depths of the underworld with their remarkable manifestations open before him. . . . After he has appeased the watchers of the kingdom of the dead and made his way past the numerous perils, he comes at last to the Lord of the Underworld, Erlik himself. And the latter rushes against him, horribly bellowing; but if the shaman is sufficiently skillful he can soothe the monster back again with promises of luxurious offerings. This moment of the dialogue with Erlik is the crisis of the ceremonial. The shaman passes into an ecstasy.

The women may be unable to locate the shaman?s position in the yonder world, in which case his spirit may fail to return to the body. Or the wandering spirit of an enemy shaman may engage him in battle or else lead him astray. It is said that there have been many shamans who failed to return.

"In every primitive tribe," writes Dr. Geza Roheim, "we find the medicine man in the center of society and it is easy to show that the medicine man is either a neurotic or a psychotic or at least that his art is based on the same mechanisms as a neurosis or a psychosis. Human groups are actuated by their group ideals, and these are always based on the infantile situation." "The infancy situation is modified or inverted by the process of maturation, again modified by the necessary adjustment to reality, yet it is there and supplies those unseen libidinal ties without which no human groups could exist." The medicine men, therefore, are simply making both visible and public the systems of symbolic fantasy that are present in the psyche of every adult member of their society. "They are the leaders in this infantile game and the lightning conductors of common anxiety. They fight the demons so that others can hunt the prey and in general fight reality."

And so it happens that if anyone -- in whatever society -- undertakes for himself the perilous journey into the darkness by descending, either intentionally or unintentionally, into the crooked lanes of his own spiritual labyrinth, he soon finds himself in a landscape of symbolical figures (any one of which may swallow him) which is no less marvelous than the wild Siberian world of the pudak and sacred mountains. In the vocabulary of the mystics,this is the second stage of the Way, that of the "purification of the self," when the senses are "cleansed and humbled," and the energies and interests "concentrated upon transcendental things"; or in a vocabulary of more modern turn: this is the process of dissolving, transcending, or transmuting the infantile images of our personal past. In our dreams the ageless perils, gargoyles, trials, secret helpers, and instructive figures are nightly still encountered; and in their forms we may see reflected not only the whole picture of our present case, but also the clue to what we must do to be saved.

"I stood before a dark cave, wanting to go in," was the dream of a patient at the beginning of his analysis; "and I shuddered at the thought that I might not be able to find my way back." "I saw one beast after another," Emanuel Swedenborg recorded in his dream book, for the night of October 19-20, 1744, "and they spread their wings, and were dragons. I was flying over them, but one of them was supporting me." And the dramatist Friedrich Hebbel recorded, a century later (April 13,1844): "In my dream I was being drawn with great force through the sea; there were terrifying abysses, with here and there a rock to which it was possible to hold." Themistocles dreamed that a snake wound itself around his body, then crept up to his neck and when it touched his face became an eagle that took him in its talons and, carrying him upward, bore him a long distance, and set him down on a golden herald's staff that suddenly appeared, so safely that he was all at once relieved of his great anxiety and fear.

The specific psychological difficulties of the dreamer frequently are revealed with touching simplicity and force:

"I had to climb a mountain. There were all kinds of obstacles in the way. I had now to jump over a ditch, now to get over a hedge, and finally to stand still because I had lost my breath." This was the dream of a stutterer.

"I stood beside a lake that appeared to be completely still. A storm came up abruptly and high waves arose, so that my whole face was splashed"; the dream of a girl afraid of blushing (ereutho-phobia), whose face, when she blushed, would become wet with perspiration.

"I was following a girl who was going ahead of me, along the dark street. I could see her from behind only and admired her beautiful figure. A mighty desire seized me, and I was running after her. Suddenly a beam, as though released from a spring, came across the street and blocked the way. I awoke with my heart pounding." The patient was a homosexual; the transverse beam, a phallic symbol.

"I got into a car, but did not know how to drive. A man who sat behind me gave me instructions. Finally, things were going quite well and we came to a plaza, where there were a number of women standing. The mother of my fiancee received me with great joy." The man was impotent, but had found an instructor in the psychoanalyst.

"A stone had broken my windshield. I was now open to the storm and rain. Tears came to my eyes. Could I ever reach my destination in this car?" The dreamer was a young woman who had lost her virginity and could not get over it.

"I saw half of a horse lying on the ground. It had only one wing and was trying to arise, but was unable to do so." The patient was a poet, who had to earn his daily bread by working as a journalist.

"I was bitten by an infant." The dreamer was suffering from a psychosexual infantilism.

"I am locked with my brother in a dark room. He has a large knife in his hand. I am afraid of him. You will drive me crazy and bring me to the madhouse, I tell him. He laughs with malicious pleasure, replying: You will always be caught with me. A chain is wrapped around the two of us. I glanced at my legs and noticed for the first time the thick iron chain that bound together my brother and myself." "The brother," comments Dr. Stekel, "was the patient's illness."

"I am going over a narrow bridge,? dreams a sixteen-year-old girl. ?Suddenly it breaks under me and I plunge into the water. An officer dives in after me, and brings me, with his strong arms, to the bank. Suddenly it seems to me then that I am a dead body. The officer too looks very pale, like a corpse."

"The dreamer is absolutely abandoned and alone in a deep hole of a cellar. The walls of his room keep getting narrower and narrower, so that he cannot stir." In this image are combined the ideas of mother womb, imprisonment, cell, and grave.

"I am dreaming that I have to go through endless corridors. Then I remain for a long time in a little room that looks like the bathing pool in the public baths. They compel me to leave the pool, and I have to pass again through a moist, slippery shaft, until I come through a little latticed door into the open. I feel like one newly born, and I think: ?This means a spiritual rebirth for me, through my analysis.?"

There can be no question: the psychological dangers through which earlier generations were guided by the symbols and spiritual exercises of their mythological and religious inheritance, we today (in so far as we are unbelievers, or, if believers, in so far as our inherited beliefs fail to represent the real problems of contemporary life) must face alone, or, at best, with only tentative, impromptu, and not often very effective guidance. This is our problem as modern, "enlightened" individuals, for whom all gods and devils have been rationalized out of existence. Nevertheless, in the multitude of myths and legends that have been preserved to us, or collected from the ends of the earth, we may yet see delineated something of our still human course. To hear and profit, however, one may have to submit somehow to purgation and surrender. And that is part of our problem: just how to do that. "Or do ye think that ye shall enter the Garden of Bliss without such trials as came to those who passed away before you?"

The oldest recorded account of the passage through the gates of metamorphosis is the Sumerian myth of the goddess Inanna?s descent to the nether world.

> From the "great above" she set her mind toward the "great below,"

> The goddess, from the "great above" she set her mind toward the "great below,"

> Inanna,from the "great above" she set her mind toward the "great below."

> My lady abandoned heaven, abandoned earth,

> To the nether world she descended,

> Inanna abandoned heaven, abandoned earth,

> To the nether world she descended.

> Abandoned lordship, abandoned ladyship,

> To the nether world she descended.

She adorned herself with her queenly robes and jewels. Seven divine decrees she fastened at her belt. She was ready to enter the "land of no return," the nether world of death and darkness, governed by her enemy and sister goddess, Ereshkigal. In fear, lest her sister should put her to death, Inanna instructed Ninshubur, her messenger, to go to heaven and set up a hue and cry for her in the assembly hall of the gods if after three days she should have failed to return.

Inanna descended. She approached the temple made of lapis lazuli, and at the gate was met by the chief gatekeeper, who demanded to know who she was and why she had come. "I am the queen of heaven, the place where the sun rises," she replied. "If thou art the queen of heaven," he said, "the place where the sun rises, why, pray, hast thou come to the land of no return? On the road whose traveler returns not, how has thy heart led thee?" Inanna declared that she had come to attend the funeral rites of her sister's husband, the lord Gugalanna; whereupon Neti, the gatekeeper, bid her stay until he should report to Ereshkigal. Neti was instructed to open to the queen of heaven the seven gates, but to abide by the custom and remove at each portal a part of her clothing.  

To the pure Inanna he says:

> "Come, Inanna, enter."

> Upon her entering the first gate,

> The shugurra, the "crown of the plain" of her head, was removed. "What, pray, is this?"

> Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world. ?

> Upon her entering the second gate,

> The rod of lapis lazuli was removed.

> "What, pray, is this?"

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

> Upon her entering the third gate,

> The small lapis lazuli stones of her neck were removed. ?What, pray, is this??

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

> Upon her entering the fourth gate,

> The sparkling stones of her breast were removed.

> "What, pray, is this?"

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

> Upon her entering the fifth gate,

> The gold ring of her hand was removed.

> "What, pray, is this?"

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

> Upon her entering the sixth gate.

> The breastplate of her breast was removed.

> "What, pray, is this?"

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

> Upon her entering the seventh gate,

> All the garments of ladyship of her body were removed. "What, pray, is this?"

> "Extraordinarily, 0 Inanna, have the decrees of the nether world been perfected,

> 0 Inanna, do not question the rites of the nether world."

Naked, she was brought before the throne. She bowed low. The seven judges of the nether world, the Anunnaki, sat before the throne of Ereshkigal, and they fastened their eyes upon Inannai? the eyes of death.

> At their word, the word which tortures the spirit,

> The sick woman was turned into a corpse,

> The corpse was hung from a stake.

Inanna and Ereshkigal, the two sisters, light and dark respectively, together represent, according to the antique manner of symbolization, the one goddess in two aspects; and their confrontation epitomizes the whole sense of the difficult road of trials. The hero, whether god or goddess, man or woman, the figure in a myth or the dreamer of a dream, discovers and assimilates his opposite (his own unsuspected self) either by swallowing it or by being swallowed. One by one the resistances are broken. He must put aside his pride, his virtue, beauty, and life, and bow or submit to the absolutely intolerable. Then he finds that he and his opposite are not of differing species, but one flesh.

The ordeal is a deepening of the problem of the first threshold and the question is still in balance: Can the ego put itself to death? For many-headed is this surrounding Hydra; one head cut off, two more appear -- unless the right caustic is applied to the mutilated stump. The original departure into the land of trials represented only the beginning of the long and really perilous path of initiatory conquests and moments of illumination. Dragons have now to be slain and surprising barriers passed?again, again, and again. Meanwhile there will be a multitude of preliminary victories, unretainable ecstasies, and momentary glimpses of the wonderful land.

## ii. The Meeting with the Goddess

The ultimate adventure, when all the barriers and ogres have been overcome, is commonly represented as a mystical marriage (ie os ydfxos) of the triumphant hero-soul with the Queen Goddess of the World. This is the crisis at the nadir, the zenith, or at the uttermost edge of the earth, at the central point of the cosmos, in the tabernacle of the temple, or within the darkness of the deepest chamber of the heart.

In the west of Ireland they still tell the tale of the Prince of the Lonesome Isle and the Lady of Tubber Tintye. Hoping to heal the Queen of Erin, the heroic youth had undertaken to go for three bottles of the water of Tubber Tintye, the flaming fairy well. Following the advice of a supernatural aunt whom he encountered on the way, and riding a wonderful, dirty, lean little shaggy horse that she gave to him, he crossed a river of fire and escaped the touch of a grove of poison trees. The horse with the speed of the wind shot past the end of the castle of Tubber Tintye; the prince sprang from its back through an open window, and came down inside, safe and sound.

> The whole place, enormous in extent, was filled with sleeping giants and monsters of sea and land---great whales, long slippery eels, bears, and beasts of every form and kind. The prince passed through them and over them till he came to a great stairway. At the head of the stairway he went into a chamber, where he found the most beautiful woman he had ever seen, stretched on a couch asleep. 'I'll have nothing to say to you,' thought he, and went on to the next; and so he looked into twelve chambers. In each was a woman more beautiful than the one before. But when he reached the thirteenth chamber and opened the door, the flash of gold took the sight from his eyes. He stood awhile till the sight came back, and then entered. In the great bright chamber was a golden couch, resting on wheels of gold. The wheels turned continually; the couch went round and round, never stopping night or day. On the couch lay the Queen of Tubber Tintye; and if her twelve maidens were beautiful, they would not be beautiful if seen near her. At the foot of the couch was Tubber Tintye itself---the well of fire. There was a golden cover upon the well, and it went around continually with the couch of the Queen.

> "Upon my word," said the prince, "I'll rest here a while." And he went up on the couch and never left it for six days and nights.

The Lady of the House of Sleep is a familiar figure in fairy tale and myth. We have already spoken of her, under the forms of Brynhild and little Briar-rose. She is the paragon of all paragons of beauty, the reply to all desire, the bliss-bestowing goal of every hero's earthly and unearthly quest. She is mother, sister, mistress, bride. Whatever in the world has lured, whatever has seemed to promise joy, has been premonitory of her existence -- in the deep of sleep, if not in the cities and forests of the world. For she is the incarnation of the promise of perfection; the soul's assurance that, at the conclusion of its exile in a world of organized inadequacies, the bliss that once was known will be known again: the comforting, the nourishing, the "good" mother -- young and beautiful -- who was known to us, and even tasted, in the remotest past. Time sealed her away, yet she is dwelling still, like one who sleeps in timelessness, at the bottom of the timeless sea.

The remembered image is not only benign, however; for the "bad" mother too -- 

1. the absent, unattainable mother, against whom aggressive fantasies are directed, and from whom a counteraggression is feared; 

2. the hampering, forbidding, punishing mother; 

3. the mother who would hold to herself the growing child trying to push away; and finally

4. the desired but forbidden mother (Oedipus complex) whose presence is a lure to dangerous desire (castration complex)

persists in the hidden land of the adult's infant recollection and is sometimes even the greater force. She is at the root of such unattainable great goddess figures as that of the chaste and terrible Diana---whose absolute ruin of the young sportsman Actaeon illustrates what a blast of fear is contained in such symbols of the mind's and body's blocked desire.

Actaeon chanced to see the dangerous goddess at noon; that fateful moment when the sun breaks in its youthful, strong ascent, balances, and begins the mighty plunge to death. He had left his companions to rest, together with his blooded dogs, after a morning of running game, and without conscious purpose had gone wandering, straying from his familiar hunting groves and fields, exploring through the neighboring woods. He discovered a vale, thick grown with cypresses and pine. He penetrated curiously into its fastness. There was a grotto within in, watered by a gentle, purling spring and with a stream that widened to a grassy pool. This shaded nook was the resort of Diana, and at that moment she was bathing among her nymphs, absolutely naked. She had put aside her hunting spear, her quiver, her unstrung bow, as well as her sandals and her robe. And one of the nude nymphs had bound up her tresses into a knot; some of the others were pouring water from capacious urns.

When the young, roving male broke into the pleasant haunt, a shriek of female terror went up, and all the bodies crowded about their mistress, trying to hide her from the profane eye.

But she stood above them, head and shoulders. The youth had seen, and was continuing to see. She glanced for her bow, but it was out of reach, so she quickly took up what was at hand, namely water, and flung it into Actaeon's face. "Now you are free to tell, if you can," she cried at him angrily, "that you have seen the goddess nude."

Antlers sprouted on his head. His neck grew great and long, his eartips sharp. His arms lengthened to legs, and his hands and feet became hooves. Terrified, he bounded---marveling that he should move so rapidly. But when he paused for breath and drink and beheld his features in a clear pool, he reared back aghast.

A terrible fate then befell Actaeon. His own hounds, catching the scent of the great stag, came baying through the wood. In a moment of joy at hearing them he paused, but then spontaneously took fright and fled. The pack followed, gradually gaining. When they had come to his heels, the first of them flying at his flank, he tried to cry their names, but the sound in his throat was not human. They fixed him with their fangs. He went down, and his own hunting companions, shouting encouragement at the dogs, arrived in time to deliver the coup de grace. Diana, miraculously aware of the flight and death, could now rest appeased.

The mythological figure of the Universal Mother imputes to the cosmos the feminine attributes of the first, nourishing and protecting presence. The fantasy is primarily spontaneous; for there exists a close and obvious correspondence between the attitude of the young child toward its mother and that of the adult toward the surrounding material world. But there has been also, in numerous religious traditions, a consciously controlled pedagogical utilization of this archetypal image for the purpose of the purging, balancing, and initiation of the mind into the nature of the visible world.

In the Tantric books of medieval and modern India the abode of thegoddess is called Mani-dvipa, "The Island of Jewels." Her couch-and-throne is there, in a grove of wish-fulfilling trees. The beaches of the isle are of golden sands. They are laved by the still waters of the ocean of the nectar of immortality. The goddess is red with the fire of life; the earth, the solar system, the galaxies of far-extending space, all swell within her womb. For she is the world creatrix, ever mother, ever virgin. She encompasses the encompassing, nourishes the nourishing, and is the life of everything that lives.

She is also the death of everything that dies. The whole round of existence is accomplished within her sway, from birth, through adolescence, maturity, and senescence, to the grave. She is the womb and the tomb: the sow that eats her farrow. Thus she unites the "good" and the "bad," exhibiting the two modes of the remembered mother, not as personal only, but as universal. The devotee is expected to contemplate the two with equal equanimity. Through this exercise his spirit is purged of its infantile, inappropriate sentimentalities and resentments, and his mind opened to the inscrutable presence which exists, not primarily as "good" and "bad" with respect to his childlike human convenience, his weal and woe, but as the law and image of the nature of being.

The great Hindu mystic of the last century, Ramakrishna (1836---1886), was a priest in a temple newly erected to the Cosmic Mother at Dakshineswar, a suburb of Calcutta. The temple image displayed the divinity in her two aspects simultaneously, the terrible and the benign. Her four arms exhibited the symbols of her universal power: the upper left hand brandishing a bloody saber, the lower gripping by the hair a severed human head; the upper right was lifted in the "fear not" gesture, the lower extended in bestowal of boons. As necklace she wore a garland of human heads; her kilt was a girdle of human arms; her long tongue was out to lick blood. She was Cosmic Power, the totality of the universe, the harmonization of all the pairs of opposites, combining wonderfully the terror of absolute destruction with an impersonal yet motherly reassurance. As change, the river of time, the fluidity of life, the goddess at once creates, preserves, and destroys. Her name is Kali, the Black One; her title: The Ferry across the Ocean of Existence.

One quiet afternoon Ramakrishna beheld a beautiful woman ascend from the Ganges and approach the grove in which he was meditating. lie perceived that she was about to give birth to a child. In a moment the babe was born, and she gently nursed it. Presently, however, she assumed a horrible aspect, took the infant in her now ugly jaws and crushed it, chewed it. Swallowing it, she returned again to the Ganges, where she disappeared.

Only geniuses capable of the highest realization can support the full revelation of the sublimity of this goddess. For lesser men she reduces her effulgence and permits herself to appear in forms concordant with their undeveloped powers. Fully to behold her would be a terrible accident for any person not spiritually prepared: as witness the unlucky case of the lusty young buck Actaeon. No saint was he, but a sportsman unprepared for the revelation of the form that must be beheld without the normal human (i.e., infantile) over- and undertones of desire, surprise, and fear.

Woman, in the picture language of mythology, represents the totality of what can be known. The hero is the one who comes to know. As he progresses in the slow initiation which is life, the form of the goddess undergoes for him a series of transfigurations: she can never be greater than himself, though she can always promise more than he is yet capable of comprehending. She lures, she guides, she bids him burst his fetters. And if he can match her import, the two, the knower and the known, will be released from every limitation. Woman is the guide to the sublime acme of sensuous adventure. By deficient eyes she is reduced to inferior states; by the evil eye of ignorance she is spellbound to banality and ugliness. But she is redeemed by the eyes of understanding. The hero who can take her as she is, without undue commotion but with the kindness and assurance she requires, is potentially the king, the incarnate god, of her created world.

A story, for example, is told of the five sons of the Irish king Eochaid: of how, having gone one day ahunting, they found themselves astray, shut in on every hand. Thirsty, they set off, one by one, to look for water. Fergus was the first: 

> and he lights on a well, over which he finds an old woman standing sentry. The fashion of the hag is this: blacker than coal every joint and segment of her was, from crown to ground; comparable to a wild horse's tail the grey wiry mass of hair that pierced her scalp's upper surface; with her sickle of a greenish looking tusk that was in her head, and curled till it touched her ear, she could lop the verdant branch of an oak in full bearing; blackened and smoke-bleared eyes she had; nose awry, wide-nostrilled; a wrinkled and freckled belly, variously unwholesome; warped crooked shins, garnished with massive ankles and a pair of capacious shovels; knotty knees she had and livid nails. The beldame's whole description in fact was disgusting. 'That's the way it is, is it?' said the lad, and 'that's the very way,' she answered. 'Is it guarding the well thou art?' he asked, and she said: 'it is.' 'Dost thou licence me to take away some water?' 'I do,' she consented, 'yet only so that I have of thee one kiss on my cheek.' 'Not so,' said he. 'Then water shall not be conceded by me.' 'My word I give,' he went on, 'that sooner than give thee a kiss I would perish of thirst!' Then the young man departed to the place where his brethren were, and told them that he had not gotten water.

Olioll, Brian, and Fiachra, likewise, went on the quest and equally attained to the identical well. Each solicited the old thing for water, but denied her the kiss.

Finally it was Niall who went, and he came to the very well.

> 'Let me have water, woman!' he cried. 'I will give it,' said she, 'and bestow on me a kiss.' He answered: 'forby giving thee a kiss, I will even hug thee!' Then he bends to embrace her, and gives her a kiss. Which operation ended, and when he looked at her, in the whole world was not a young woman of gait more graceful, in universal semblance fairer than she: to be likened to the last-fallen snow lying in trenches every portion of her was, from crown to sole; plump and queenly forearms, fingers long and taper, straight legs of a lovely hue she had; two sandals of the white bronze betwixt her smooth and soft white feet and the earth; about her was an ample mantle of the choicest fleece pure crimson, and in the garment a brooch of white silver; she had lustrous teeth of pearl, great regal eyes, mouth red as the rowanberry. 'Here, woman, is a galaxy of charms,' said the young man. 'That is true indeed.' 'And who art thou?' he pursued. "'Royal Rule" am I,' she answered, and uttered this:

"King of Tara! I am Royal Rule...."

"Go now," she said, "to thy brethren, and take with thee water; moreover, thine and thy children's for ever the kingdom and supreme power shall be....And as at the first thou hast seen me ugly, brutish, loathly -- in the end, beautiful -- even so is royal rule: for without battles, without fierce conflict, it may not be won; but in the result, he that is king of no matter what shows comely and handsome forth."

Such is royal rule? Such is life itself. The goddess guardian of the inexhaustible well -- whether as Fergus, or as Actaeon, or as the Prince of the Lonesome Isle discovered her -- requires that the hero should be endowed with what the troubadours and minnesingers termed the "gentle heart." Not by the animal desire of an Actaeon, not by the fastidious revulsion of such as Fergus, can she be comprehended and rightly served, but only by gentleness: *aware* ("gentle sympathy") it was named in the romantic courtly poetry of tenth- to twelfth-century Japan.

> Within the gentle heart Love shelters himself.

> As birds within the green shade of the grove.

> Before the gentle heart, in nature's scheme,

> Love was not, nor the gentle heart ere Love.

> For with the sun, at once,

> So sprang the light immediately; nor was 

> Its birth before the sun's.

> And Love hath his effect in gentleness Of very self; even as

> Within the middle fire the heat's excess.

The meeting with the goddess (who is incarnate in every woman) is the final test of the talent of the hero to win the boon of love (charity: *amor fati*), which is life itself enjoyed as the encasement of eternity.

And when the adventurer, in this context, is not a youth but a maid, she is the one who, by her qualities, her beauty, or her yearning, is fit to become the consort of an immortal. Then the heavenly husband descends to her and conducts her to his bed -- whether she will or no. And if she has shunned him, the scales fall from her eyes; if she has sought him, her desire finds its peace.

The Arapaho girl who followed the porcupine up the stretching tree was enticed to the camp-circle of the people of the sky. There she became the wife of a heavenly youth. It was he who, under the form of the luring porcupine, had seduced her to his supernatural home.

The king's daughter of the nursery tale, the day following the adventure at the well, heard a thumping at her castle door: the frog had arrived to press her to her bargain. And in spite of her great disgust, he followed her to her chair at table, shared the meal from her little golden plate and cup, even insisted on going to sleep with her in her little silken bed. In a tantrum she plucked him from the floor and flung him at the w r all. When he fell, he was no frog but a king's son with kind and beautiful eyes. And then we hear that they were married and were driven in a beautiful coach back to the young man's waiting kingdom, where the two became a king and queen.

Or once again: when Psyche had accomplished all of the difficult tasks, Jupiter himself gave to her a draft of the elixir of immortality; so that she is now and forever united with Cupid, her beloved, in the paradise of perfected form.

The Greek Orthodox and Roman Catholic churches celebrate the same mystery in the Feast of the Assumption:

"The Virgin Mary is taken up into the bridal chamber of heaven, where the King of Kings sits on his starry throne."

"O Virgin most prudent, whither goest thou, bright as the morn? all beautiful and sweet art thou, O daughter of Zion, fair as the moon, elect as the sun."


## iii. Woman as the Temptress

The mystical marriage with the queen goddess of the world represents the hero's total mastery of life; for the woman is life, the hero its knower and master. And the testings of the hero, which were preliminary to his ultimate experience and deed, were symbolical of those crises of realization by means of which his consciousness came to be amplified and made capable of enduring the full possession of the mother-destroyer, his inevitable bride. With that he knows that he and the father are one: he is in the father's place.

Thus phrased, in extremest terms, the problem may sound remote from the affairs of normal human creatures. Nevertheless, every failure to cope with a life situation must be laid, in the end, to a restriction of consciousness. Wars and temper tantrums are the makeshifts of ignorance; regrets are illuminations come too late. The whole sense of the ubiquitous myth of the hero's passage is that it shall serve as a general pattern for men and women, wherever they may stand along the scale. Therefore it is formulated in the broadest terms. The individual has only to discover his own position with reference to this general human formula, and let it then assist him past his restricting walls. Who and where are his ogres? Those are the reflections of the unsolved enigmas of his own humanity. What are his ideals? Those are the symptoms of his grasp of life.

In the office of the modern psychoanalyst, the stages of the hero-adventure come to light again in the dreams and hallucinations of the patient. Depth beyond depth of self-ignorance is fathomed, with the analyst in the role of the helper, the initiatory priest. And always, after the first thrills of getting under way, the adventure develops into a journey of darkness, horror, disgust, and phantasmagoric fears.

The crux of the curious difficulty lies in the fact that our conscious views of what life ought to be seldom correspond to what life really is. Generally we refuse to admit within ourselves, or within our friends, the fullness of that pushing, self-protective, malodorous, carnivorous, lecherous fever which is the very nature of the organic cell. Rather, we tend to perfume, whitewash, and reinterpret; meanwhile imagining that all the flies in the ointment, all the hairs in the soup, are the faults of some unpleasant someone else.

But when it suddenly dawns on us, or is forced to our attention, that everything we think or do is necessarily tainted with the odor of the flesh, then, not uncommonly, there is experienced a moment of revulsion: life, the acts of life, the organs of life, woman in particular as the great symbol of life, become intolerable to the pure, the pure, pure soul.

> 0, that this too too solid flesh would melt.

> Thaw and resolve itself into a dew!

> Or that the Everlasting had not fix'd

> His canon 'gainst self-slaughter! 0 God! God!

So exclaims the great spokesman of this moment, Hamlet:

> How weary, stale, flat, and unprofitable Seem to me all the uses of this world!

> Fie on't! ah fie! 'tis an unweeded garden,

> That grows to seed; things rank and gross in nature Possess it merely. That it should come to this!

The innocent delight of Oedipus in his first possession of the queen turns to an agony of spirit when he learns who the woman is. Like Hamlet, he is beset by the moral image of the father. Like Hamlet, he turns from the fair features of the world to search the darkness for a higher kingdom than this of the incest and adultery ridden, luxurious and incorrigible mother. The seeker of the life beyond life must press beyond her, surpass the temptations of her call, and soar to the immaculate ether beyond.

> For a God called him -- called him many times,

> From many sides at once: "Ho, Oedipus,

> Thou Oedipus, why are we tarrying?

> It is full long that thou art stayed for; come!"

Where this Oedipus-Hamlet revulsion remains to beset the soul, there the world, the body, and woman above all, become the symbols no longer of victory but of defeat. A monasticpuritanical, world-negating ethical system then radically and immediately transfigures all the images of myth. No longer can the hero rest in innocence with the goddess of the flesh; for she is become the queen of sin.

"So long as a man has any regard for this corpse-like body," writes the Hindu monk Shankaracharya, "he is impure, and suffers from his enemies as well as from birth, disease and death; but when he thinks of himself as pure, as the essence of the Good, and the Immovable, he becomes free.... Throw far away this limitation of a body which is inert and filthy by nature. Think of it no longer. For a thing that has been vomited (as you should vomit forth your body) can excite only disgust when it is recalled again to mind."

This is a point of view familiar to the West from the lives and writings of the saints.

"When Saint Peter observed that his daughter, Petronilla, was too beautiful, he obtained from God the favor that she should fall sick of a fever. Now one day when his disciples were by him, Titus said to him: 'You who cure all maladies, why do you not fix it so that Petronilla can get up from her bed?' And Peter replied to him: 'Because I am satisfied with her condition as it is.' This was by no means as much as to say he had not the power to cure her; for, immediately, he said to her: 'Get up, Petronilla, and make haste to wait on us.' The young girl, cured, got up and came to serve them. But when she had finished, her father said to her: 'Petronilla, return to your bed!' She returned, and was straightway taken again with the fever. And later, when she had begun to be perfect in her love of God, her father restored her to perfect health.

"At that time a noble gentleman named Flaccus, struck by her beauty, came to bid for her hand in marriage. She replied: 'If you wish to marry me, send a group of young girls to conduct me to your home!' But when these had arrived, Petronilla directly set herself to fast and pray. Receiving communion, she lay back in her bed, and after three days rendered her soul up to God."

"As a child Saint Bernard of Clairvaux suffered from headaches. A young woman came to visit him one day, to soothe his sufferings with her songs. But the indignant child drove her from the room. And God rewarded him for his zeal; for he got up from his bed immediately, and was cured.

"Now the ancient enemy of man, perceiving little Bernard to be of such wholesome disposition, exerted himself to set traps for his chastity. When the child, however, at the instigation of the devil, one day had stood staring at a lady for some time, suddenly he blushed for himself, and entered the icy water of a pond in penance, where he remained until frozen to his bones. Another time, w r hen he was asleep, a young girl came naked to his bed. Bernard, becoming aware of her, yielded in silence the part of the bed in which he lay, and rolling over to the other side returned to sleep. Having stroked and caressed him for some time, the unhappy creature presently was so taken with shame, in spite of her shamelessness, that she got up and fled, full of horror at herself and of admiration for the young man.

"Still again, when Bernard together with some friends one time had accepted the hospitality of the home of a certain wealthy lady, she, observing his beauty, was seized with a passion to sleep with him. She arose that night from her bed, and came and placed herself at the side of her guest. But he, the minute he felt someone close to him, began to shout: 'Thief! Thief!' Immediately thereupon, the woman scurried away, the entire house was  on its feet, lanterns were lit, and everybody hunted around for the burglar. But since none was found, all returned to their beds and to sleep, with the sole exception of this lady, who, unable to close her eyes, again arose and slipped into the bed of her guest. Bernard began to shout: 'Thief!' And again the alarms and investigations! After that, even a third time this lady brought herself to be spurned in like fashion; so that finally she gave up her wicked project, out of either fear or discouragement. On the road next day the companions of Bernard asked him why he had had so many dreams about thieves. And he replied to them: 'I had really to repel the attacks of a thief; for my hostess tried to rob me of a treasure, which, had I lost it, I should never have been able to regain.'

"All of which convinced Bernard that it was a pretty risky thing to live together with the serpent. So he planned to be quit of the world and enter the monastic order of the Cistercians."

Not even monastery walls, however, not even the remoteness of the desert, can defend against the female presences; for as long as the hermit's flesh clings to his bones and pulses warm, the images of life are alert to storm his mind. Saint Anthony, practising his austerities in the Egyptian Thebaid, was troubled by voluptuous hallucinations perpetrated by female devils attracted to his magnetic solitude. Apparitions of this order, with loins of irresistible attraction and breasts bursting to be touched, are known to all the hermit-resorts of history. u Ah! bel ermite! bel ermite! \... Si tu posais ton doigt sur mon epaule, ce serait comme une trainee de feu dans tes veines. La possession de la moindre place de mon corps t'emplira d'unejoie plus vehemente que la conquete d'un empire. Avance tes levres....'

Writes the New Englander, Cotton Mather: u The Wilderness through which we are passing to the Promised Land is all filled with Fiery flying serpents. But, blessed be God, none of them have hitherto so fastened upon us as to confound us utterly! All our way to Heaven lies by Dens of Lions and the Mounts of Leopards ; there are incredible Droves of Devils in our way\.... We are poor travellers in a world which is as well the Devil's Field, as the Devil's Gaol-, a world in which every Nook whereof, the Devil is encamped with Bands of Robbers to pester all that have their faces looking Zionward."

## iv. Atonement with the Father

"The Bow of God's Wrath is bent, and the Arrow made ready on the String and Justice bends the Arrow at your Heart, and strains the Bow; and it is nothing but the mere Pleasure of God, and that of an angry God, without any Promise or Obligation at all, that keeps the Arrow one Moment from being made drunk with your Blood...."

With these words Jonathan Edwards threatened the hearts of his New England congregation by disclosing to them, unmitigated, the ogre aspect of the father. He riveted them to the pews with images of the mythological ordeal; for though the Puritan prohibited the graven image, yet he allowed himself the verbal. "The Wrath," Jonathan Edwards thundered,

> the Wrath of God is like great Waters that are dammed for the present; they increase more and more, and rise higher and higher till an Outlet is given; and the longer the Stream is stopt, the more rapid and mighty is its Course when once it is let loose. 'Tis true, that Judgment against your evil Works has not been executed hitherto; the Floods of God's Vengeance have been withheld; but your Guilt in the mean Time is constantly increasing, and you are every Day treasuring up more Wrath; the Waters are continually rising, and waxing more and more mighty; and there is nothing but the mere Pleasure of God that holds the Waters back that are unwilling to be stopt, and press hard to go forward; if God should only withdraw his Hand from the Flood-gate, it would immediately fly open, and the fiery Floods of the Fierceness of the Wrath of God would rush forth with inconceivable Fury, and would come upon you with omnipotent Power; and if your Strength were Ten thousand Times greater than it is, yea Ten thousand Times greater than the Strength of the stoutest, sturdiest Devil in Hell, it would be nothing to withstand or endure it....

Having threatened with the element water, Pastor Jonathan next turns to the image of fire. 

> The God that holds you over the Pit of Hell, much as one holds a Spider or some lothsome Insect over the Fire, abhors you, and is dreadfully provoked; his Wrath towards you burns like Fire; he looks upon you as Worthy of nothing else but to be cast into the Fire; he is of purer Eyes than to bear to have you in his Sight; you are Ten thousand Times so abominable in his Eyes as the most hateful venomous Serpent is in ours. You have offended him infinitely more than ever a stubborn Rebel did his Prince; and yet 'tis nothing but his Hand that holds you from falling into the Fire every Moment....

> O Sinner! ... You hang by a slender Thread, with the Flames of Divine Wrath flashing about it, and ready every Moment to singe it, and burn it asunder; and you have no Interest in any Mediator, and nothing to lay hold of to save yourself, nothing to keep off the Flames of Wrath, nothing of your own, nothing that you ever have done, nothing that you can do, to induce God to spare you one Moment...."

But now, at last, the great resolving image of the second birth---only for a moment, however:

> Thus are all you that never passed under a great Change of Heart, by the mighty Power of the Spirit of GOD upon your souls, all that were never born again, and made new Creatures, and raised from being dead in Sin, to a State of new, and before altogether unexperienced Light and Life (however you may have reformed your Life in many Things, and may have had religious Affections, and may keep up a Form of Religion in your Families and Closets, and in the House of God, and may be strict in it) you are thus in the Hands of an angry God; 'tis nothing but his mere Pleasure that keeps you from being this Moment swallowed up in everlasting Destruction.

"God's mere pleasure," which defends the sinner from the arrow, the flood, and the flames, is termed in the traditional vocabulary of Christianity God's "mercy"; and "the mighty power of the spirit of God," by which the heart is changed, that is God's "grace." In most mythologies, the images of mercy and grace are rendered as vividly as those of justice and wrath, so that a balance is maintained, and the heart is buoyed rather than scourged along its way. "Fear not!" says the hand gesture of the god Shiva, as he dances before his devotee the dance of the universal destruction . 46 "Fear not, for all rests well in God. The forms that come and go ---and of which your body is but oneare the flashes of my dancing limbs. Know Me in all, and of what shall you be afraid?" The magic of the sacraments (made

---

The symbolism of this eloquent image has been well expounded by Ananda K. Coomaraswamy, The Dance of Siva (New York, 1917), pp. 56-66, and by Heinrich Zimmer, Myths and Symbols in Indian Art and Civilization , pp. 151-175. Briefly: the extended right hand holds the drum, the beat of which is the beat of time, time being the first principle of creation; the extended left holds the flame, which is the flame of the destruction of the created world; the second right hand is held in the gesture of "fear not," while the second left, pointing to the lifted left foot, is held in a position symbolizing "elephant" (the elephant is the "breaker of the way through the jungle of the world," i.e., the divine guide); the right foot is planted on the back of a dwarf, the demon "Non-knowing," which signifies the passage of souls from God into matter, but the left is lifted, showing the release of the soul: the left is the foot to which the "elephant-hand" is pointing and supplies the reason for the assurance, "Fear not." The God's head is balanced, serene and still, in the midst of the dynamism of creation and destruction which is symbolized by the rocking arms and the rhythm of the slowly stamping right heel. This means that at the center all is still. Shiva's right earring is a man's, his left, a woman's; for the God includes and is beyond the pairs of opposites. Shiva's facial expression is neither sorrowful nor joyous, but is the visage of the Unmoved Mover, beyond, yet present within, the world's bliss and pain. The wildly streaming locks represent the long-untended hair of the Indian Yogi, now flying in the dance of life; for the presence known in the joys and sorrows of life, and that found through withdrawn meditation, are but two aspects of the same, universal, non-dual, Being-Consciousness-Bliss. Shiva's bracelets, arm bands, ankle rings, effective through the passion of Jesus Christ, or by virtue of the meditations of the Buddha), the protective power of primitive amulets and charms, and the supernatural helpers of the myths and fairy tales of the world, are mankind's assurances that the arrow, the flames, and the flood are not as brutal as they seem. and brahminical thread,\* are living serpents. This means that he is made beautiful by the Serpent Power---the mysterious Creative Energy of God, which is the material and the formal cause of his own self-manifestation in, and as, the universe with all its beings. In Shiva's hair may be seen a skull, symbolic of death, the forehead-ornament of the Lord of Destruction, as well as a crescent moon, symbolic of birth and increase, which are his other boons to the world. Also, there is in his hair the flower of a datura---from which plant an intoxicant is prepared (compare the wine of Dionysos and the wine of the Mass). A little image of the goddess Ganges is hidden in his locks; for it is he who receives on his head the impact of the descent of the divine Ganges from heaven, letting the life- and salvation-bestowing waters then flow gently to the earth for the physical and spiritual refreshment of mankind. The dance posture of the God may be visualized as the symbolic syllable AUM 3ft or which is the verbal equivalent of the four states of consciousness and their fields of experience. (A: waking consciousness; U: dream consciousness; M: dreamless sleep; the silence around the sacred syllable is the Unmanifest Transcendent For a discussion of this syllable, cf, infra , pp. 245-247, and note 16, p. 257.) The God is thus within the worshiper as well as without.

Such a figure illustrates the function and value of a graven image, and shows why long sermons are unnecessary among idol-worshipers. The devotee is permitted to soak in the meaning of the divine symbol in deep silence and in his own good time. Furthermore, just as the god wears arm bands and ankle rings, so does the devotee; and these mean what the god's mean. They are made of gold instead of serpents, gold (the metal that does not corrode) symbolizing immortality; i.e., immortality is the mysterious creative energy of God, which is the beauty of the body.

Many other details of life and local custom are similarly duplicated, interpreted, and thus validated, in the details of the anthropomorphic idols. In this way, the whole of life is made into a support for meditation. One lives in the midst of a silent sermon all the time.

\* The brahminical thread is a cotton thread worn by the members of the three upper castes (the so-called twice-born) of India. It is passed over the head and right arm, so that it rests on the left shoulder and runs across the body (chest and back) to the right hip. This symbolizes the second birth of the twice-bom, the thread itself representing the threshold, or sun door, so that the twice-bom is dwelling at once in time and eternity.

---

For the ogre aspect of the father is a reflex of the victim's own ego -- derived from the sensational nursery scene that has been left behind, but projected before; and the fixating idolatry of that pedagogical nonthing is itself the fault that keeps one steeped in a sense of sin, sealing the potentially adult spirit from a better balanced, more realistic view of the father, and therewith of the world. Atonement (at-one-ment) consists in no more than the abandonment of that self-generated double monster---the dragon thought to be God (superego) and the dragon thought to be Sin (repressed id). But this requires an abandonment of the attachment to ego itself; and that is what is difficult. One must have a faith that the father is merciful, and then a reliance on that mercy. Therewith, the center of belief is transferred outside of the bedeviling god's tight scaly ring, and the dreadful ogres dissolve.

It is in this ordeal that the hero may derive hope and assurance from the helpful female figure, by whose magic (pollen charms or power of intercession) he is protected through all the frightening experiences of the father's ego-shattering initiation. For if it is impossible to trust the terrifying father-face, then one's faith must be centered elsewhere (Spider Woman, Blessed Mother); and with that reliance for support, one endures the crisis---only to find, in the end, that the father and mother reflect each other, and are in essence the same.

When the Twin Warriors of the Navaho, having departed from Spider Woman with her advice and protective charms, had made their perilous way between the rocks that crush, through the reeds that cut to pieces, and the cactus plants that tear to pieces, and then across the boiling sands, they came at last to the house of the Sun, their father. The door was guarded by two bears. These arose and growled; but the words that Spider Woman had taught the boys made the animals crouch down again. After the bears, there threatened a pair of serpents, then winds, then lightnings: the guardians of the ultimate threshold. All were readily appeased, however, with the words of the prayer.

Built of turquoise, the house of the Sun was great and square, and it stood on the shore of a mighty water. The boys entered it, and they beheld a woman sitting in the west, two handsome young men in the south, two handsome young women in the north. The young women stood up without a word, wrapped the newcomers in four sky-coverings, and placed them on a shelf. The boys lay quietly. Presently a rattle hanging over the door shook four times and one of the young women said, "Our father is coming."

The bearer of the sun strode into his home, removed the sun from his back, and hung it on a peg on the west wall of the room, where it shook and clanged for some time, going "tla, tla, tla, tla." He turned to the older woman and demanded angrily: "Who were those two that entered here today?" But the woman did not reply. The young people looked at one another. The bearer of the sun put his question angrily four times before the woman said to him at last: "It would be well for you not to say too much. Two young men came hither today, seeking their father. You have told me that you pay no visits when you go abroad, and that you have met no woman but me. Whose sons, then, are these?" She pointed to the bundle on the shelf, and the children smiled significantly at one another.

The bearer of the sun took the bundle from the shelf, unrolled the four robes (the robes of dawn, blue sky, yellow evening light, and darkness), and the boys fell out on the floor. He immediately seized them. Fiercely, he flung them at some great sharp spikes of white shell that stood in the east. The boys tightly clutched their life-feathers and bounded back. The man hurled them, equally, at spikes of turquoise in the south, haliotis in the west, and black rock in the north . 49 The boys always

---

Four symbolical colors, representing the points of the compass, play a prominent role in Navaho iconography and cult. They are white, blue, yellow, and black, signifying, respectively, east, south, west, and north. These correspond to the red, white, green, and black on the hat of the African trickster divinity Edshu (see p. 42, supra); for the House of the Father, like the Father himself, symbolizes the Center.

The Twin Heroes are tested against the symbols of the four directions, to discover whether they partake of the faults and limitations of any one of the quarters.

---

clutched their life-feathers tightly and came bounding back. "I wish it were indeed true," said the Sun, "that they were my children."

The terrible father assayed then to steam the boys to death in an overheated sweatlodge. They were aided by the winds, who provided a protected retreat within the lodge in which to hide. "Yes, these are my children," said the Sun when they emerged--- but that was only a ruse; for he was still planning to trick them. The final ordeal was a smoking-pipe filled with poison. A spiny caterpillar warned the boys and gave them something to put into their mouths. They smoked the pipe without harm, passing it back and forth to one another till it was finished. They even said it tasted sweet. The Sun was proud. He was completely satisfied. "Now, my children," he asked, "what is it you want from me? Why do you seek me?" The Twin Heroes had won the full confidence of the Sun, their father.

The need for great care on the part of the father, admitting to his house only those who have been thoroughly tested, is illustrated by the unhappy exploit of the lad Phaethon, described in a famous tale of the Greeks. Born of a virgin in Ethiopia and taunted by his playmates to search the question of his father, he set off across Persia and India to find the palace of the Sun---for his mother had told him that his father was Phoebus, the god who drove the solar chariot.

"The palace of the Sun stood high on lofty columns, bright with glittering gold and bronze that shone like fire. Gleaming ivory crowned the gables above; the double folding doors were radiant with burnished silver. And the workmanship was more beautiful than the materials."

Climbing the steep path, Phaethon arrived beneath the roof. And he discovered Phoebus sitting on an emerald throne, surrounded by the Hours and the Seasons, and by Day, Month, Year, and Century. Hie bold youngster had to halt at the threshold, his mortal eyes unable to bear the light; but the father gently spoke to him across the hall.

"Why have you come?" the father asked. "What do you seek, O Phaethon---a son no father need deny?"

The lad respectfully replied: "O my father (if thou grantest me the right to use that name)! Phoebus! Light of the entire world! Grant me a proof, my father, by which all may know me for thy true son."

The great god set his glittering crown aside and bade the boy approach. He gathered him into his arms. Then he promised, sealing the promise with a binding oath, that any proof the lad desired would be granted.

What Phaethon desired was his father's chariot, and the right to drive the winged horses for a day.

"Such a request," said the father, "proves my promise to have been rashly made." He put the boy a little away from him and sought to dissuade him from the demand, "In your ignorance," said he, "you are asking for more than can be granted even to the gods. Each of the gods may do as he will, and yet none, save myself, has the power to take his place in my chariot of fire; no, not even Zeus."

Phoebus reasoned. Phaethon was adamant. Unable to retract the oath, the father delayed as long as time would allow, but was finally forced to conduct his stubborn son to the prodigious chariot: its axle of gold and the pole of gold, its wheels with golden tires and a ring of silver spokes. The yoke was set with chrysolites and jewels. The Hours were already leading the four horses from their lofty stalls, breathing fire and filled with ambrosial food. They put upon them the clanking bridles; the great animals pawed at the bars. Phoebus anointed Phaethon's face with an ointment to protect it against the flames and then placed on his head the radiant crown.

"If, at least, you can obey your father's warnings," the divinity advised, "spare the lash and hold tightly to the reins. The horses go fast enough of themselves. And do not follow the straight road directly through the five zones of heaven, but turn off at the fork to the left -- the tracks of my wheels you will clearly see. Furthermore, so that the sky and earth may have equal heat, be careful to go neither too high nor too low; for if you go too high you will burn up the skies, and if you go too low ignite the earth. In the middle is the safest path.

"But hurry! While I am speaking, dewy Night has reached her goal on the western shore. We are summoned. Behold, the dawn is glowing. Boy, may Fortune aid and conduct you better than you can guide yourself. Here, grasp the reins."

Tethys, the goddess of the sea, had dropped the bars, and the horses, with a jolt, abruptly started; cleaving with their feet the clouds; beating the air with their wings; outrunning all the winds that were rising from the same eastern quarter. Immediately---the chariot was so light without its accustomed weight--- the car began to rock about like a ship tossing without ballast on the waves. The driver, panic-stricken, forgot the reins, and knew nothing of the road. Wildly mounting, the team grazed the heights of the sky and startled the remotest constellations. The Great and Little Bear were scorched. The Serpent lying curled about the polar stars grew warm, and with the heat grew dangerously fierce. Bootes took flight, encumbered with his plough. The Scorpion struck with his tail.

The chariot, having roared for some time through unknown regions of the air, knocking against the stars, next plunged down crazily to the clouds just above the ground; and the Moon beheld, in amazement, her brother's horses running below her own. The clouds evaporated. The earth burst into flame. Mountains blazed; cities perished with their walls; nations were reduced to ashes. That was the time the peoples of Ethiopia became black; for the blood was drawn to the surface of their bodies by the heat. Libya became a desert. The Nile fled in terror to the ends of the earth and hid its head, and it is hidden yet.

Mother Earth, shielding her scorched brow with her hand, choking with hot smoke, lifted her great voice and called upon Jove, the father of all things, to save his world. "Look around!" she cried at him. "The  heavens are asmoke from pole to pole. Great Jove, if the sea perish, and the land, and all the realms of the sky, then we are back again in the chaos of the beginning! Take thought! Take thought for the safety of our universe! Save from the flames whatever yet remains!"

Jove, the Almighty Father, hastily summoned the gods to witness that unless some measure were quickly taken all was lost. Thereupon he hurried to the zenith, took a thunderbolt in his right hand, and flung it from beside his ear. The car shattered; the horses, terrified, broke loose; Phaethon, fire raging in his hair, descended like a falling star. And the river Po received his burning frame.

The Naiads of that land consigned his body to a tomb, whereupon this epitaph:

> Here Phaethon lies: in Phoebus' car he fared,

> And though he greatly failed, more greatly dared.

This tale of indulgent parenthood illustrates the antique idea that when the roles of life are assumed by the improperly initiated, chaos supervenes. When the child outgrows the popular idyl of the mother breast and turns to face the world of specialized adult action, it passes, spiritually, into the sphere of the father---who becomes, for his son, the sign of the future task, and for his daughter, of the future husband. Whether he knows it or not, and no matter what his position in society, the father is the initiating priest through whom the young being passes on into the larger world. And just as, formerly, the mother represented the "good" and "evil," so now does he, but with this complication---that there is a new element of rivalry in the picture: the son against the father for the mastery of the universe, and the daughter against the mother to be the mastered world.

The traditional idea of initiation combines an introduction of the candidate into the techniques, duties, and prerogatives of his vocation with a radical readjustment of his emotional relationship to the parental images. The mystagogue (father or fathersubstitute) is to entrust the symbols of office only to a son who has been effectually purged of all inappropriate infantile cathexes---for whom the just, impersonal exercise of the powers will not be rendered impossible by unconscious (or perhaps even conscious and rationalized) motives of self-aggrandizement, personal preference, or resentment. Ideally, the invested one has been divested of his mere humanity and is representative of an impersonal cosmic force. He is the twice-born: he has become himself the father. And he is competent, consequently, now to enact himself the role of the initiator, the guide, the sun door, through whom one may pass from the infantile illusions of "good" and "evil" to an experience of the majesty of cosmic law, purged of hope and fear, and at peace in the understanding of the revelation of being.

"Once I dreamed," declared a little boy, "that I was captured by cannon balls \[sic\]. They all began to jump and yell. I was surprised to see myself in my own parlor. There was a fire, and a kettle was over it full of boiling water. They threw me into it and once in a while the cook used to come over and stick a fork into me to see if I was cooked. Then he took me out and gave me to the chief, who was just going to bite me when I woke up."

"I dreamed that I was at table with my wife," states a civilized gentleman. "During the course of the meal I reached over and took our second child, a baby, and in a matter-of-fact fashion proceeded to put him into a green soup bowl, full of hot water or some hot liquid; for he came out cooked thoroughly, like chicken fricassee.

"I laid the viand on a bread board at the table and cut it up with my knife. When we had eaten all of it except a small part like a chicken gizzard I looked up, worried, to my wife and asked her, 'Are you sure you wanted me to do this? Did you intend to have him for supper?'

"She answered, with a domestic frown, 'After he was so well cooked, there was nothing else to do.' I was just about to finish the last piece, when I woke up."

This archetypal nightmare of the ogre father is made actual in the ordeals of primitive initiation. The boys of the Australian Murngin tribe, as we have seen, are first frightened and sent running to their mothers. The Great Father Snake is calling for their foreskins. This places the women in the role of protectresses.

A prodigious horn is blown, named Yurlunggur, which is supposed to be the call of the Great Father Snake, who has emerged from his hole. When the men come for the boys, the women grab up spears and pretend not only to fight but also to wail and cry, because the little fellows are going to be taken away and "eaten." The men's triangular dancing ground is the body of the Great Father Snake. There the boys are shown, during many nights, numerous dances symbolical of the various totem ancestors, and are taught the myths that explain the existing order of the world. Also, they are sent on a long journey to neighboring and distant clans, imitative of the mythological wanderings of the phallic ancestors. In this way, "within" the Great Father Snake as it were, they are introduced to an interesting new object world that compensates them for their loss of the mother; and the male phallus, instead of the female breast, is made the central point (axis mundi) of the imagination.

The culminating instruction of the long series of rites is the release of the boy's own hero-penis from the protection of its foreskin, through the frightening and painful attack upon it of the circumciser: 

---

It is interesting to note the continuance to this day of the rite of circumcision in the Hebrew and Mohammedan cults, where the feminine element has been scrupulously purged from the official, strictly monotheistic mythology. "God forgiveth not the sin of joining other gods with Him," we read in the Koran. "The Pagans, leaving Allah, call but upon female deities" (Koran, 4:116, 117).

---

Among the Arunta, for example, the sound of the bull-roarers is heard from all sides when the moment has arrived for this decisive break from the past. It is night, and in the weird light of the fire suddenly appear the circumciser and his assistant. The noise of the bull-roarers is the voice of the great demon of the ceremony, and the pair of operators are its apparition. With their beards thrust into their mouths, signifying anger, their legs widely extended, and their arms stretched forward, the two men stand perfectly still, the actual operator in front, holding in his right hand the small flint knife with which the operation is to be conducted, and his assistant pressing close up behind him, so that the two bodies are in contact with each other. Then a man approaches through the firelight, balancing a shield on his head and at the same time snapping the thumb and first finger of each hand. The bull-roarers are making a tremendous din, which can be heard by the women and children in their distant camp. The man with the shield on his head goes down on one knee just a little in front of the operator, and immediately one of the boys is lifted from the ground by a number of his uncles, who carry him feet foremost and place him on the shield, while in deep, loud tones a chant is thundered forth by all the men. The operation is swiftly performed, the fearsome figures retire immediately from the lighted area, and the boy, in a more or less dazed condition, is attended to, and congratulated by the men to whose estate he has now just arrived. "You have done well," they say; "you did not cry out."

The native Australian mythologies teach that the first initiation rites were carried out in such a way that all the young men were killed.

The ritual is thus shown to be, among other things, a dramatized expression of the Oedipal aggression of the elder generation; and the circumcision, a mitigated castration . 59 But the rites provide also for the cannibal, patricidal impulse of the younger, rising group of males, and at the same time reveal the benign selfgiving aspect of the archetypal father; for during the long period of symbolical instruction, there is a time when the initiates are forced to live only on the fresh-drawn blood of the older men. "The natives," we are told, "are particularly interested in the Christian communion rite, and having heard about it from missionaries they compare it to the blood-drinking rituals of their own."

"In the evening the men come and take their places according to tribal precedence, the boy lying with his head on his father's thighs. He must make no movement or he will die. The father blindfolds him with his hands because if the boy should witness the following proceedings it is believed that his father and mother will both die. The wooden vessel or a bark vessel is placed near one of the boy's mother's brothers, who, having tied his arm lightly, pierces the upper part with a nosebone and holds the arm over the vessel until a certain amount of blood has been taken. The man next to him pierces his arm, and so on, until the vessel is filled. It may hold two quarts or so. The boy takes a long draught of the blood. Should his stomach rebel, the father holds his throat to prevent his ejecting the blood, because if it happens his father, mother, sisters, and brothers would all die. The remainder of the blood is thrown over him.

"From this time on, sometimes for a whole moon, the boy is allowed no other food than human blood, Yamminga, the mythical ancestor, having made this law. . . . Sometimes the blood is dried in the vessel and then the guardian cuts it in sections with his nose bone, and it is eaten by the boy, the two end sections first. The sections must be regularly divided or the boy will die ."

Frequently the men who give their blood faint and remain in a state of coma for an hour or more because of exhaustion . 62 "In former times," writes another observer, "this blood (drunk ceremonially by the novices) was obtained from a man who was killed for the purpose, and portions of his body were eaten ." 63 "Here," comments Dr. Roheim, "we come as near to a ritual representation of the killing and eating of the primal father as we can ever get."

There can be no doubt that no matter how unilluminated the stark-naked Australian savages may seem to us, their symbolical ceremonials represent a survival into modern times of an incredibly old system of spiritual instruction, the far-flung evidences of which are to be found not only in all the lands and islands bordering the Indian Ocean, but also among the remains of the archaic centers of what we tend to regard as our own very special brand of civilization. Just how much the old men know, it is difficult to judge from the published accounts of our Occidental observers. But it can be seen from a comparison of the figures of Australian ritual with those familiar to us from higher cultures, that the great themes, the ageless archetypes, and their operation upon the soul remain the same.

and the other men all raised a death cry. The boys were lifeless. Theold wirreenuns (medicine men), dipping their stone knives in the blood,
touched with them the lips of all present. . . . The bodies of the
Boorah victims were cooked. Each man who had been to five Boorahs ate a
piece of this flesh; no others were allowed to see this done" (K.
Langloh Parker, The Euahlayi Tribe , 1905, pp. 72-73; cited by Roheim,
The Eternal Ones of the Dream, p. 232).

65 For an astounding revelation of the survival in contemporary
Melanesia of a symbolic system essentially identical with that of the
Egypto-Babylonian, Trojan-Cretan "labyrinth complex" of the second
millennium B.c., cf. John Layard, Stone Men of Malekula (London: Chatto
and Windus, 1942). W. F. J. Knight, in his Cumaean Gates (Oxford, 1936),
has discussed the evident relationship of the Malekulan "journey of the
soul to the underworld" with the classical descent of Aeneas, and the
Babylonian of Gilgamesh. W. J. Perry, The Children of the Sun (New York:
E. P. Dutton and Co., 1923), thought he could recognize evidences of
this culture-continuity running all the way from Egypt and Sumer out
through the Oceanic area to North America. Many scholars have pointed
out the close correspondences between the details of the classical Greek
and primitive Australian rites of initiation, notably Jane Harrison,
Themis , A Study of the Social Origins of Greek Religion (2nd revised
edition; Cambridge University Press, 1927).

It is still uncertain by what means and in what eras the mythological
and cultural patterns of the various archaic civilizations may have been
disseminated to the farthest corners of the earth; yet it can be stated
categorically that few (if any) of the so-called "primitive cultures"
studied by our anthropologists represent autochthonous growths. They
are, rather, local adaptations, provincial degenerations, and immensely
old fossilizations, of folkways that were developed in very different
lands, often under much less simple circumstances, and by other races.

Come, 0 Dithyrambos,

Enter this my male womb.

This cry of Zeus, the Thunder-hurler, to the child, his son, Dionysos,
sounds the leitmotif of the Greek mysteries of the initiatory second
birth. "And bull-voices roar thereto from somewhere out of the unseen,
fearful semblances, and from a drum an image as it were of thunder
underground is borne on the air heavy with dread." 67 The word
"Dithyrambos" itself, as an epithet of the killed and resurrected
Dionysos, was understood by the Greeks to signify "him of the double
door," him who had survived the awesome miracle of the second birth. And
we know that the choral songs (dithyrambs) and dark, blood-reeking rites
in celebration of the god---associated with the renewal of vegetation,
the renewal of the moon, the renewal of the sun, the renewal of the
soul, and solemnized at the season of the resurrection of the year
god---represent the ritual beginnings of the Attic tragedy. Throughout
the ancient world such myths and rites abounded: the deaths and
resurrections of Tammuz, Adonis, Mithra, Virbius, Attis, and Osiris, and
of their various animal representatives (goats and sheep, bulls, pigs,
horses, fish, and birds) are known to every student of comparative,
religion; the popular carnival games of the Whitsuntide Louts, Green
Georges, John Barleycorns, and Kostrubonkos, Carrying-out-Winter,
Bringingin-Summer, and Killing of the Christmas Wren, have continued the
tradition, in a mood of frolic, into our contemporary calendar; 68 and
through the Christian church (in the mythology of the Fall and
Redemption, Crucifixion and Resurrection, the "second birth" of baptism,
the initiator}\' blow on the cheek at confirmation, the symbolical eating of the Flesh and drinking of the
Blood) solemnly, and sometimes effectively, we are united to those
immortal images of initiatory might, through the sacramental operation
of which, man, since the beginning of his day on earth, has dispelled
the terrors of his phenomenality and won through to the
all-transfiguring vision of immortal being. "For if the blood of bulls
and of goats, and the ashes of an heifer sprinkling the unclean,
sanctifieth to the purifying of the flesh: how much more shall the blood
of Christ, who through the eternal Spirit offered himself without spot
to God, purge your conscience from dead works to serve the living God?"
69

ITiere is a folktale told by the Basumbwa of East Africa, of a man whose
dead father appeared to him, driving the cattle of Death, and conducted
him along a path that went into the ground, as into a vast burrow. They
came to an extensive area where there were some people. The father hid
the son and went off to sleep. The Great Chief, Death, appeared the next
morning. One side of him was beautiful; but the other side was rotten,
maggots were dropping to the ground. His attendants were gathering the
maggots. The attendants washed the sores, and when they had finished,
Death said: "The one born today: if he goes trading, he will be robbed.
The woman who conceives today: she will die with the child conceived.
The man who cultivates today: his crops have perished. The one who is to
go into the jungle has been eaten by the lion."

Death thus pronounced the universal curse, and returned to rest. But the
following morning, when he appeared, his attendants washed and perfumed
the beautiful side, massaging it with oil. When they had finished, Death
pronounced the blessing: "The one born today: may he become wealthy. May
the woman who conceives today give birth to a child who will live to be
old. The one born today: let him go into the market; may he strike good
bargains; may he trade with the blind. The man who is to enter the
jungle: may he kill game; may he discover even elephants. Because today
I pronounce the benediction."

The father then said to the son: "If you had arrived today, many things
would have come into your possession. But now it is clear that poverty
has been ordained for you. Tomorrow you had better go."

And the son returned to his home. 70

The Sun in the Underworld, Lord of the Dead, is the other side of the
same radiant king who rules and gives the day; for "Who is it that
sustains you from the sky and from the earth? And who is it that brings
out the living from the dead and the dead from the living? And who is it
that rules and regulates all affairs?" 71 We recall the Wachaga tale of
the very poor man, Kyazimba, who was transported by a crone to the
zenith, where the Sun rests at noon; 72 there the Great Chief bestowed
on him prosperity. And we recall the trickster-god Edshu, described in a
tale from the other coast of Africa: 73 spreading strife was his
greatest joy. These are differing views of the same dreadful Providence.
In him are contained and from him proceed the contradictions, good and
evil, death and life, pain and pleasure, boons and deprivation. As the
person of the sun door, he is the fountainhead of all the pairs of
opposites. "With Him are the keys of the Unseen\.... In the end, unto
Him will be your return; then will He show you the truth of all that ye
did." 74

The mystery of the apparently self-contradictory father is rendered
tellingly in the figure of a great divinity of prehistoric Peru, named
Viracocha. His tiara is the sun; he grasps a thunderbolt in either hand;
and from his eyes descend, in the form of tears, the rains that refresh
the life of the valleys of the world. Viracocha is the Universal God,
the creator of all things; and yet, in the legends of his appearances
upon the earth, he is shown

wandering as a beggar, in rags and reviled. One is reminded of the
Gospel of Mary and Joseph at the inn-doors of Bethlehem, 75 and of the
classical story of the begging of Jove and Mercury at the home of Baucis
and Philemon. 76 One is reminded also of the unrecognized Edshu. This is
a theme frequently encountered in mythology; its sense is caught in the
words of the Koran: "wither-soever ye turn, there is the Presence of
Allah." 7 \' "Though He is hidden in all things," say the Hindus, "that
Soul shines not forth; yet He is seen by subtle seers with superior,
subtle intellect." 78 "Split the stick," runs a Gnostic aphorism, "and
there is Jesus."

Viracocha, therefore, in this manner of manifesting his ubiquity,
participates in the character of the highest of the universal gods.
Furthermore his synthesis of sun-god and storm-god is familiar. We know
it through the Hebrew mythology of Yahweh, in whom the traits of two
gods are united (Yahweh, a storm-god, and El, a solar); it is apparent
in the Navaho personification of the father of the Twin Warriors; it is
obvious in the character of Zeus, as well as in the thunderbolt and halo
of certain forms of the Buddha image. The meaning is that the grace that
pours into the universe through the sun door is the same as the energy
of the bolt that annihilates and is itself indestructible: the
delusion-shattering light of the Imperishable is the same as the light
that creates. Or again, in terms of a secondary polarity of nature: the
fire blazing in the sun glows also in the fertilizing storm; the energy
behind the elemental pair of opposites, fire and water, is one and the
same.

But the most extraordinary and profoundly moving of the traits of
Viracocha, this nobly conceived Peruvian rendition of the universal god,
is the detail that is peculiarly his own, namely that of the tears. The
living waters are the tears of God. Herewith the world-discrediting
insight of the monk, "All life is

sorrowful," is combined with the world-begetting affirmative of the
father: "Life must be!" In full awareness of the life anguish of the
creatures of his hand, in full consciousness of the roaring wilderness
of pains, the brain-splitting fires of the deluded, selfravaging,
lustful, angry universe of his creation, this divinity acquiesces in the
deed of supplying life to life. To withhold the seminal waters would be
to annihilate; yet to give them forth is to create this world that we
know. For the essence of time is flux, dissolution of the momentarily
existent; and the essence of life is time. In his mercy, in his love for
the forms of time, this demiurgic man of men yields countenance to the
sea of pangs; but in his full awareness of what he is doing, the seminal
waters of the life that he gives are the tears of his eyes.

The paradox of creation, the coming of the forms of time out of
eternity, is the germinal secret of the father. It can never be quite
explained. Therefore, in every system of theology there is an umbilical
point, an Achilles tendon which the finger of mother life has touched,
and where the possibility of perfect knowledge has been impaired. The
problem of the hero is to pierce himself (and therewith his world)
precisely through that point; to shatter and annihilate that key knot of
his limited existence.

The problem of the hero going to meet the father is to open his soul
beyond terror to such a degree that he will be ripe to understand how
the sickening and insane tragedies of this vast and ruthless cosmos are
completely validated in the majesty of Being. The hero transcends life
with its peculiar blind spot and for a moment rises to a glimpse of the
source. He beholds the face of the father, understands---and the two are
atoned.

In the Biblical story of Job, the Lord makes no attempt to justify in
human or any other terms the ill pay meted out to his virtuous servant,
"a simple and upright man, and fearing God, and avoiding evil." Nor was
it for any sins of their own that Job's servants were slain by the
Chaldean troops, his sons and daughters crushed by a collapsing roof.
When his friends arrive to console him, they declare, with a pious faith
in God's justice, that Job must have done some evil to have deserved to
be so frightfully afflicted. But the honest, courageous,
horizon-searching

[]{#chap_0013.xhtml#221 epub="http://www.idpf.org/2007/ops"
type="pagebreak" title="221"}

sufferer insists that his deeds have been good; whereupon the comforter,
Elihu, charges him with blasphemy, as naming himself more just than God.

When the Lord himself answers Job out of the whirlwind, He makes no
attempt to vindicate His work in ethical terms, but only magnifies His
Presence, bidding Job do likewise on earth in human emulation of the way
of heaven: "Gird up thy loins now like a man; I will demand of thee, and
declare thou unto me. Wilt thou also disannul my judgment? Wilt thou
condemn me, that thou mayst be righteous? Hast thou an arm like God? or
canst thou thunder with a voice like him? Deck thyself now with majesty
and excellency; and array thyself with glory and beauty. Cast abroad the
rage of thy wrath: and behold every one that is proud and abase him.
Look on every one that is proud, and bring him low; and tread down the
wicked in their place. Hide them in the dust together; and bind their
faces in secret. Then I will also confess unto thee that thine own hand
can save thee." 79

There is no word of explanation, no mention of the dubious wager with
Satan described in chapter one of the Book of Job; only a
thunder-and-lightning demonstration of the fact of facts, namely that
man cannot measure the will of God, which derives from a center beyond
the range of human categories. Categories, indeed, are totally shattered
by the Almighty of the Book of Job, and remain shattered to the last.
Nevertheless, to Job himself the revelation appears to have made
soul-satisfying sense. He was a hero who, by his courage in the fiery
furnace, his unreadiness to break down and grovel before a popular
conception of the character of the All Highest, had proven himself
capable of facing a greater revelation than the one that satisfied his
friends. We cannot interpret his words of the last chapter as those of a
man merely intimidated. They are the words of one who has seen something
surpassing anything that has been said by way of justification. "I have
heard of thee by the hearing of the ear: but now mine eye seeth thee.
Wherefore I abhor myself, and repent

in dust and ashes." 80 The pious comforters are humbled; Job is rewarded
with a fresh house, fresh servants, and fresh daughters and sons. "After
this lived Job an hundred and forty years, and saw his sons, and his
sons' sons, even four generations. So Job died, being old and full of
days." 81

For the son who has grown really to know the father, the agonies of the
ordeal are readily borne; the world is no longer a vale of tears but a
bliss-yielding, perpetual manifestation of the Presence. Contrast with
the wrath of the angry God known to Jonathan Edwards and his flock, the
following tender lyric from the miserable east-European ghettos of that
same century:

Oh, Lord of the Universe

I will sing Thee a song.

Where canst Thou be found,

And where canst Thou not be found?

Where I pass---there art Thou.

Where 1 remain---there, too, Thou art.

Thou, Thou, and only Thou.

Doth it go well---'tis thanks to Thee.

Doth it go ill---ah, 'tis also thanks to Thee.

Thou art, Thou hast been, and Thou wilt be.

Thou didst reign, Thou reignest, and Thou wilt reign.

Thine is Heaven, Thine is Earth.

Thou fillest the high regions,

And Thou fillest the low regions.

Wheresoever I turn, Thou, oh Thou, art there , 82

======

## v. Apotheosis

One of the most powerful and beloved of the Bodhisattvas of the Mahayana
Buddhism of Tibet, China, and Japan is the Lotus Bearer,
Avalokiteshvara, "The Lord Looking Down in Pity," so called because he
regards with compassion all sentient creatures suffering the evils of
existence. 83 To him goes the millionfoldrepeated prayer of the prayer
wheels and temple gongs of Tibet: Om mani padme hum, "The jewel is in
the lotus." To him go perhaps more prayers per minute than to any single
divinity known to man; for when, during his final life on earth as a
human being, he shattered for himself the bounds of the last threshold
(which moment opened to him the timelessness of the void beyond the
frustrating mirage-enigmas of the named and bounded cosmos), he paused:
he made a vow that before entering the void he would bring all creatures
without exception to enlightenment; and since then he has permeated the
whole texture of existence with the divine grace of his assisting
presence, so that the least prayer addressed to him, throughout the vast

spiritual empire of the Buddha, is graciously heard. Under differing
forms he traverses the ten thousand worlds, and appears in the hour of
need and prayer. He reveals himself in human form with two arms, in
superhuman forms with four arms, or with six, or twelve, or a thousand,
and he holds in one of his left hands the lotus of the world.

Like the Buddha himself, this godlike being is a pattern of the divine
state to which the human hero attains who has gone beyond the last
terrors of ignorance. "When the envelopment of consciousness has been
annihilated, then he becomes free of all fear, beyond the reach of
change ." 84 This is the release potential within us all, and which
anyone can attain---through herohood; for, as we read: "All things are
Buddha-things "; 85 or again (and this is the other way of making the
same statement): "All beings are without self."

The world is filled and illumined by, but does not hold, the Bodhisattva
("he whose being is enlightenment"); rather, it is he who holds the
world, the lotus. Pain and pleasure do not enclose him, he encloses
them---and with profound repose. And since he is what all of us may be,
his presence, his image, the mere naming of him, helps. "He wears a
garland of eight thousand rays, in which is seen fully reflected a state
of perfect beauty. The color of his body is purple gold. His palms have
the mixed color of five hundred lotuses, while each finger tip has
eightyfour thousand signet-marks, and each mark eighty-four thousand
colors; each color has eighty-four thousand rays which are soft and mild
and shine over all things that exist. With these jewel hands he draws
and embraces all beings. The halo surrounding his head is studded with
five hundred Buddhas, miraculously transformed, each attended by five
hundred Bodhisattvas, who are attended, in turn, by numberless gods. And
when he puts his feet down to the ground, the flowers of diamonds and
jewels that are scattered cover everything in all directions. The color
of

his face is gold. While in his towering crown of gems stands a Buddha,
two hundred and fifty miles high." 86

In China and Japan this sublimely gentle Bodhisattva is represented not
only in male form, but also as female. Kwan Yin of China, Kwannon of
Japan---the Madonna of the Far East---is precisely this benevolent
regarder of the world. She will be found in every Buddhist temple of the
farthest Orient. She is blessed alike to the simple and to the wise; for
behind her vow there lies a profound intuition, world-redeeming,
world-sustaining. The pause on the threshold of Nirvana, the resolution
to forego until the end of time (which never ends) immersion in the
untroubled pool of eternity, represents a realization that the
distinction between eternity and time is only apparent---made, perforce,
by the rational mind, but dissolved in the perfect knowledge of the mind
that has transcended the pairs of opposites. What is understood is that
time and eternity are two aspects of the same experience-whole, two
planes of the same nondual ineffable; i.e., the jewel of eternity is in
the lotus of birth and death: om mani padme hum.

The first wonder to be noted here is the androgynous character of the
Bodhisattva: masculine Avalokiteshvara, feminine Kwan Yin. Male-female
gods are not uncommon in the world of myth. They emerge always with a
certain mystery; for they conduct the mind beyond objective experience
into a symbolic realm where duality is left behind. Awonawilona, chief
god of the pueblo of Zuni, the maker and container of all, is sometimes
spoken of as he, but is actually he-she. The Great Original of the
Chinese chronicles, the holy woman T'ai Yuan, combined in her person the
masculine Yang and the feminine Yin . 87 The cabalistic teachings of

the medieval Jews, as well as the Gnostic Christian writings of the
second century, represent the Word Made Flesh as androgynous-which was
indeed the state of Adam as he was created, before the female aspect,
Eve, was removed into another form. And among the Greeks, not only
Hermaphrodite (the child of Hermes and Aphrodite ), 88 but Eros too, the
divinity of love (the first of the gods, according to Plato ), 89 were
in sex both female and male.

"So God created man in his own image, in the image of God created he
him; male and female created he them ." 90 The question may arise in the
mind as to the nature of the image of God; but the answer is already
given in the text, and is clear enough. "When the Holy One, Blessed be
He, created the first man, He created him androgynous ." 91 The removal
of the feminine into another form symbolizes the beginning of the fall
from perfection into duality; and it was naturally followed by the
discovery of the duality of good and evil, exile from the garden where
God walks on earth, and thereupon the building of the wall of Paradise,
constituted of the "coincidence of opposites ," 92 by which Man (now man
and woman) is cut off from not only the vision but even the recollection
of the image of God.

This is the Biblical version of a myth known to many lands. It
represents one of the basic ways of symbolizing the mystery of creation:
the devolvement of eternity into time, the breaking of the one into the
two and then the many, as well as the generation of new life through the
reconjunction of the two. This image

stands at the beginning of the cosmogonic cycle , 93 and with equal
propriety at the conclusion of the hero-task, at the moment when the
wall of Paradise is dissolved, the divine form found and recollected,
and wisdom regained . 94

Tiresias, the blinded seer, was both male and female: his eyes were
closed to the broken forms of the light-world of the pairs of opposites,
yet he saw in his own interior darkness the destiny of Oedipus . 95
Shiva appears united in a single body with Shakti, his spouse-he the
right side, she the left-in the manifestation known as Ardhanarisha,
"The Half-Woman Lord ." 96 The ancestral images of certain African and
Melanesian tribes show on one being the breasts of the mother and the
beard and penis of the father . 97 And in Australia, about a year
following the ordeal of the circumcision, the candidate for full manhood
undergoes a second ritual operation-that of subincision (a slitting open
of the underside of the penis, to form a permanent cleft into the
urethra). The opening is termed the "penis womb." It is a symbolical
male vagina. The hero has become, by virtue of the ceremonial, more than
man . 98

The blood for ceremonial painting and for gluing white bird's-down to
the body is derived by the Australian fathers from their subincision
holes. They break open again the old

wounds, and let it flow.\" It symbolizes at once the menstrual blood of
the vagina and the semen of the male, as well as urine, water, and male
milk. The flowing shows that the old men have the source of life and
nourishment within themselves ; 100 i.e., that they and the
inexhaustible world fountain are the same . 101

The call of the Great Father Snake was alarming to the child; the mother
was protection. But the father came. He was the guide and initiator into
the mysteries of the unknown. As the original intruder into the paradise
of the infant with its mother, the father is the archetypal enemy;
hence, throughout life all enemies are symbolical (to the unconscious)
of the father. "Whatever is killed becomes the father ." 102 Hence the
veneration in headhunting communities (in New Guinea, for example) of
the heads brought home from vendetta raids . 103 Hence, too, the
irresistible compulsion to make war: the impulse to destroy the father
is continually transforming itself into public violence. The old men of
the immediate community or race protect themselves from their growing
sons by the psychological magic of their totem ceremonials. They enact
the ogre father, and then reveal themselves to be the feeding mother
too. A new and larger paradise is thus established. But this paradise
does not include the traditional enemy tribes, or races, against w r hom
aggression is still systematically projected. All of the "good"
father-mother content

is saved for home, while the "bad" is flung abroad and about: "for who
is this uncircumcised Philistine, that he should defy the armies of the
living God ?" 104 "And slacken not in following up the enemy: if ye are
suffering hardships, they are suffering similar hardships; but ye have
hope from Allah, while they have none ." 105

Totem, tribal, racial, and aggressively missionizing cults represent
only partial solutions of the psychological problem of subduing hate by
love; they only partially initiate. Ego is not annihilated in them;
rather, it is enlarged; instead of thinking only of himself, the
individual becomes dedicated to the whole of his society. The rest of
the world meanwhile (that is to say, by far the greater portion of
mankind) is left outside the sphere of his sympathy and protection
because outside the sphere of the protection of his god. And there takes
place, then, that dramatic divorce of the two principles of love and
hate which the pages of history so bountifully illustrate. Instead of
clearing his own heart the zealot tries to clear the world. The laws of
the City of God are applied only to his in-group (tribe, church, nation,
class, or what not) while the fire of a perpetual holy war is hurled
(with good conscience, and indeed a sense of pious service) against
whatever uncircumcised, barbarian, heathen, "native," or alien people
happens to occupy the position of neighbor . 106

The world is full of the resultant mutually contending bands: totem-,
flag-, and party-worshipers. Even the so-called Christian nations-which
are supposed to be following a "World" Redeemer-are better known to
history for their colonial barbarity and internecine strife than for any
practical display of that unconditioned love, synonymous with the
effective conquest of ego, ego's world, and ego's tribal god, which was
taught by their professed supreme Lord: "I say unto you, Love your
enemies, do good to them which hate you. Bless them that curse you, and

pray for them which despitefully use you. And unto him that smiteth thee
on the one cheek offer also the other; and him that taketh away thy
cloke forbid not to take thy coat also. Give to every man that asketh of
thee; and of him that taketh away thy goods ask them not again. And as
ye would that men should do to you, do ye also to them likewise. For if
ye love them which love you, what thank have ye? for sinners also love
those that love them. And if ye do good to them which do good to you,
what thank have ye? for sinners also do even the same. And if ye lend to
them of whom ye hope to receive, what thank have ye? for sinners also
lend to sinners, to receive as much again. But love ye your enemies, and
do good, and lend, hoping for nothing again; and your reward shall be
great, and ye shall be the children of the Highest: for he is kind unto
the unthankful and to the evil. Be ye therefore merciful, as your Father
also is merciful ." 107

Once we have broken free of the prejudices of our own provincially
limited ecclesiastical, tribal, or national rendition of the world
archetypes, it becomes possible to understand that the supreme
initiation is not that of the local motherly fathers, who then project
aggression onto the neighbors for their own defense.

---

Compare the following Christian letter:

In the Tear of Our Lord 1682

To ye aged and beloved, Mr. John Higginson:

There be now at sea a ship called Welcome, which has on board 100 or
more of the heretics and malignants called Quakers, with W. Penn, who is
the chief scamp, at the head of them. The General Court has accordingly
given sacred orders to Master Malachi Huscott, of the brig Porpoise, to
waylay the said Welcome slyly as near the Cape of Cod as may be, and
make captive the said Penn and his ungodly crew, so that the Lord may be
glorified and not mocked on the soil of this new country with the
heathen worship of these people. Much spoil can be made of selling the
whole lot to Barbadoes, where slaves fetch good prices in rum and sugar
and we shall not only do the Lord great good by punishing the wicked,
but we shall make great good for His Minister and people.

Yours in the bowels of Christ,

Cotton Mathf.r

(Reprinted by Professor Robert Phillips, American Government and Its
Problems, Houghton Mifflin Company, 1941, and by Dr. Karl Menninger,
Love Against Hate, Harcourt, Brace and Company, 1942, p. 211.)

---

The good news, which the World Redeemer brings and which so many have
been glad to hear, zealous to preach, but reluctant, apparently, to
demonstrate, is that God is love, that He can be, and is to be, loved,
and that all without exception are his children . 108 Such comparatively
trivial matters as the remaining details of the credo, the techniques of
worship, and devices of episcopal organization (which have so absorbed
the interest of Occidental theologians that they are today seriously
discussed as the principal questions of religion ), 109 are merely
pedantic snares, unless kept ancillary to the major teaching. Indeed,
where not so kept, they have a regressive effect: they reduce the father
image back again to the dimensions of the totem. And this, of course, is
what has happened throughout the Christian world. One would think that
we had been called upon to decide or to know whom, of all of us, the
Father prefers. Whereas, the teaching is much less flattering: u Judge
not, that ye be not judged ." 110 The World Savior's cross, in spite of
the behavior of its professed priests, is a vastly more democratic
symbol than the local flag . 111

The understanding of the final-and critical-implications of the
world-redemptive words and symbols of the tradition of Christendom has
been so disarranged, during the tumultuous centuries that have elapsed
since St. Augustine's declaration of the holy war of the Civitas Dei
against the Civitas Diaboli, that the modern thinker wishing to know the
meaning of a world religion (i.e., of a doctrine of universal love) must
turn his mind to the other great (and much older) universal communion:
that of the Buddha, where the primary word still is peace---peace to all
beings. 112

The following Tibetan verses, for example, from two hymns of the
poet-saint Milarepa, were composed about the time that Pope Urban II was
preaching the First Crusade:

Amid the City of Illusoriness of the Six World-Planes

The chief factor is the sin and obscuration born of evil works;

Therein the being followeth dictates of likes and dislikes,

Andfindeth ne\'er the time to know Equality:

Avoid, 0 my son, likes and dislikes. 113

If ye realize the Emptiness of All Things, Compassion will arise within
your hearts;

If ye lose all differentiation between yourselves and others, fit to
serve others ye will be;

And when in serving others ye shall win success, then shall ye meet with
me;

And finding me, ye shall attain to Buddhahood. lu

112 I do not mention Islam, because there, too, the doctrine is preached
in terms of the holy war and thus obscured. It is certainly true that
there, as well as here, many have known that the proper field of battle
is not geographical but psychological (compare Rumi, Mathnawi, 2. 2525:
"What is 'beheading'? Slaying the carnal soul in the holy war.");
nevertheless, the popular and orthodox expression of both the Mohammedan
and the Christian doctrines has been so ferocious that it requires a
very sophisticated reading to discern in either mission the operation of
love.

Peace is at the heart of all because Avalokiteshvara-Kwannon, the mighty
Bodhisattva, Boundless Love, includes, regards, and dwells within
(without exception) every sentient being. The perfection of the delicate
wings of an insect, broken in the passage of time, he regards---and he
himself is both their perfection and their disintegration. The perennial
agony of man, self-torturing, deluded, tangled in the net of his own
tenuous delirium, frustrated, yet having within himself, undiscovered,
absolutely unutilized, the secret of release: this too he regards---and
is. Serene above man, the angels; below man, the demons and unhappy
dead: these all are drawn to the Bodhisattva by the rays of his jewel
hands, and they are he, as he is they. The bounded, shackled centers of
consciousness, myriadfold, on every plane of existence (not only in this
present universe, limited by the Milky Way, but beyond, into the reaches
of space), galaxy beyond galaxy, world beyond world of universes, coming
into being out of the timeless pool of the void, bursting into life, and
like a bubble therewith vanishing: time and time again: lives by the
multitude: all suffering: each bounded in the tenuous, tight circle of
itself---lashing, killing, hating, and desiring peace beyond victory:
these all are the children, the mad figures of the transitory yet
inexhaustible, long world dream of the All-Regarding, whose essence is
the essence of Emptiness: "The Lord Looking Down in Pity."

But the name means also: u The Lord Who is Seen Within." 115 We are all
reflexes of the image of the Bodhisattva. The sufferer

---

"The Emptiness of All Tilings" (Sanskrit: sunyatd, "voidness") refers,
on the one hand, to the illusory nature of the phenomenal world, and on
the other, to the impropriety of attributing such qualities as we may
know from our experience of the phenomenal world to the Imperishable.

In the Heavenly Radiance of the Voidness,

There existeth not shadow of thing or concept ,

Yet It pervadeth all objects of knowledge ;

Obeisance to the Immutable Voidness.

("Hymn of Milarepa in praise of his teacher," ibid. p. 137.)

---

within us is that divine being. We and that protecting father are one.
This is the redeeming insight. That protecting father is every man we
meet. And so it must be known that, though this ignorant, limited,
self-defending, suffering body may regard itself as threatened by some
other---the enemy---that one too is the God. The ogre breaks us, but the
hero, the fit candidate, undergoes the initiation "like a man"; and
behold, it was the father: we in Him and He in us. 116 The dear,
protecting mother of our body could not defend us from the Great Father
Serpent; the mortal, tangible body that she gave us was delivered into
his frightening power. But death was not the end. New life, new birth,
new knowledge of existence (so that we live not in this physique only,
but in all bodies, all physiques of the world, as the Bodhisattva) was
given us. That father was himself the womb, the mother, of a second
birth. 117

This is the meaning of the image of the bisexual god. He is the mystery
of the theme of initiation. We are taken from the mother, chewed into
fragments and assimilated to the worldannihilating body of the ogre for
whom all the precious forms and beings are only the courses of a feast;
but then, miraculously reborn, we are more than we were. If the God is a
tribal, racial, national, or sectarian archetype, we are the warriors of
his cause; but if he is a lord of the universe itself, we then go forth
as knowers to whom all men are brothers. And in either case, the
childhood parent images and ideas of "good" and "evil" have been
surpassed. We no longer desire and fear; we are what was
desired and feared. All the gods, Bodhisattvas, and Buddhas have been
subsumed in us, as in the halo of the mighty holder of the lotus of the
world.

"Come," therefore, "and let us return unto the Lord: for he hath torn,
and he will heal us; he hath smitten, and he will bind us up. After two
days will he revive us: in the third day he will raise us up, and we
shall live in his sight. Then shall we know, if we follow on to know the
Lord: his going forth is prepared as the morning; and he shall come unto
us as the rain, as the latter and former rain unto the earth." 118

This is the sense of the first wonder of the Bodhisattva: the
androgynous character of the presence. Therewith the two apparently
opposite mythological adventures come together: the Meeting with the
Goddess, and the Atonement with the Father. For in the first the
initiate learns that male and female are (as phrased in the
Brihadaranyaka Upanishad) "two halves of a split pea"; 119 whereas in
the second, the Father is found to be antecedent to the division of sex:
the pronoun "He" was a manner of speech, the myth of Sonship a guiding
line to be erased. And in both cases it is found (or rather,
recollected) that the hero himself is that which he had come to find.

Tine second wonder to be noted in the Bodhisattva myth is its
annihilation of the distinction between life and release-from-life---
which is symbolized (as we have observed) in the Bodhisattva's
renunciation of Nirvana. Briefly, Nirvana means "the Extinguishing, of
the Threefold Fire of Desire, Hostility, and Delusion." 120

1,8 Hosea, 6: 1-3.

As the reader will recall: in the legend of the Temptation under the Bo
Tree (supra, pp. 31-32) the antagonist of the Future Buddha was
Kama-Mara, literally "Desire---Hostility," or "Love and Death," the
magician of Delusion. He was a personification of the Threefold Fire and
of the difficulties of the last test, a final threshold guardian to be
passed by the universal hero on his supreme adventure to Nirvana. Having
subdued within himself to the critical point of the ultimate ember the
Threefold Fire, which is the moving power of the universe, the Savior
beheld reflected, as in a mirror all around him, the last projected
fantasies of his primitive physical will to live like other human
beings---the will to live according to the normal motives of desire and
hostility, in a delusory ambient of phenomenal causes, ends, and means.
He was assailed by the last fury of the disregarded flesh. And this was
the moment on which all depended; for from one coal could arise again
the whole conflagration.

This greatly celebrated legend affords an excellent example of the close
relationship maintained in the Orient between myth, psychology, and
metaphysics. The vivid personifications prepare the intellect for the
doctrine of the interdependence of the inner and the outer worlds. No
doubt the reader has been struck by a certain resemblance of this
ancient mythological doctrine of the dynamics of the psyche to the
teachings of the modern Freudian school. According to the latter, the
life-wish (eros or libido , corresponding to the Buddhist Kama,
"desire") and the death-wish (thanatos or destrudo, which is identical
with the Buddhist Mara, "hostility or death") are the two drives that
not only move the individual from within but also animate for him the
surrounding world. 121 Moreover, the unconsciously grounded delusions
from which desires and hostilities arise are in both systems dispelled
by psychological analysis (Sanskrit: viveka) and illumination (Sanskrit:
vidya). Yet the aims of the two teachings-the traditional and the
modern-are not exactly the same.

Psychoanalysis is a technique to cure excessively suffering individuals
of the unconsciously misdirected desires and hostilities that weave
around them their private webs of unreal terrors and ambivalent
attractions; the patient released from these finds himself able to
participate with comparative satisfaction in the more realistic fears,
hostilities, erotic and religious practices, business enterprises, wars,
pastimes, and household tasks offered to him by his particular culture.
But for the one who has deliberately undertaken the difficult and
dangerous journey beyond the village compound, these interests, too, are
to be regarded as based on error. Therefore the aim of the religious
teaching is not to cure the individual back again to the general
delusion, but to detach him from delusion altogether; and this not by
readjusting the desire ( eros ) and hostility (thanatos)-ior that would
only originate a new context of delusion-but by extinguishing the
impulses to the very root, according to the method of the celebrated
Buddhist Eightfold Path:

Right Belief, Right Intentions,

Right Speech, Right Actions,

Right Livelihood, Right Endeavoring,

Right Mindfulness, Right Concentration.

With the final "extirpation of delusion, desire, and hostility"
(Nirvana) the mind knows that it is not what it thought: thought goes.
The mind rests in its true state. And here it may dwell until the body
drops away.

Stars, darkness, a lamp, a phantom, dew, a bubble,

A dream, a flash of lightning, and a cloud:

Thus we should look upon all that was made. 122

The Bodhisattva, however, does not abandon life. Turning his regard from
the inner sphere of thought-transcending truth (which can be described
only as "emptiness," since it surpasses speech) outward again to the
phenomenal world, he perceives
without the same ocean of being that he found within. "Form is
emptiness, emptiness indeed is form. Emptiness is not different from
form, form is not different from emptiness. What is form, that is
emptiness; what is emptiness, that is form. And the same applies to
perception, name, conception, and knowledge." 123 Having surpassed the
delusions of his formerly self-assertive, self-defensive, self-concerned
ego, he knows without and within the same repose. What he beholds
without is the visual aspect of the magnitudinous, thought-transcending
emptiness on which his own experiences of ego, form, perceptions,
speech, conceptions, and knowledge ride. And he is filled with
compassion for the self-terrorized beings who live in fright of their
own nightmare. He rises, returns to them, and dwells with them as an
egoless center, through whom the principle of emptiness is made manifest
in its own simplicity. And this is his great "compassionate act"; for by
it the truth is revealed that in the understanding of one in whom the
Threefold Fire of Desire, Hostility, and Delusion is dead, this world is
Nirvana. "Gift waves" go out from such a one for the liberation of us
all. "This our worldly life is an activity of Nirvana itself, not the
slightest distinction exists between them." 124

And so it may be said that the modern therapeutic goal of the cure back
to life is attained through the ancient religious discipline, after all;
only the circle traveled by the Bodhisattva is a large one; and the
departure from the world is regarded not as a fault, but as the first
step into that noble path at the remotest turn of which illumination is
to be won concerning the deep emptiness of the universal round. Such an
ideal is well known,

also, to Hinduism: the one freed in life ( j\'ivan mukta), desireless,
compassionate, and wise, "with the heart concentrated by yoga, viewing
all things with equal regard, beholds himself in all beings and all
beings in himself. In whatever way he leads his life, that one lives in
God." 125

The story is told of a Confucian scholar who besought the twenty-eighth
Buddhist patriarch, Bodhidharma, "to pacify his soul." Bodhidharma
retorted, "Produce it and I will pacify it." The Confucian replied,
"That is my trouble, I cannot find it." Bodhidharma said, "Your wish is
granted." The Confucian understood and departed in peace. 126

Those who know, not only that the Everlasting lives in them, but that
what they, and all things, really are is the Everlasting, dwell in the
groves of the wish-fulfilling trees, drink the brew of immortality, and
listen everywhere to the unheard music of eternal concord. These are the
immortals. The Taoist landscape paintings of China and Japan depict
supremely the heavenliness of this terrestrial state. The four
benevolent animals, the phoenix, the unicorn, the tortoise, and the
dragon, dwell amongst the willow gardens, the bamboos, and the plums,
and amid the mists of sacred mountains, close to the honored spheres.
Sages, with craggy bodies but spirits eternally young, meditate among
these peaks, or ride curious, symbolic animals across immortal tides, or
converse delightfully over teacups to the flute of Lan Ts'ai-ho.

The mistress of the earthly paradise of the Chinese immortals is the
fairy goddess Hsi Wang Mu, "The Golden Mother of the Tortoise." She
dwells in a palace on the K'un-lun Mountain, which is surrounded by
fragrant flowers, battlements of jewels,
and a garden wall of gold. 127 She is formed of the pure quintessence of
the western air. Her guests at her periodical "Feast of the Peaches"
(celebrated when the peaches ripen, once in every six thousand years)
are served by the Golden Mother's gracious daughters, in bowers and
pavilions by the Lake of Gems. Waters play there from a remarkable
fountain. Phoenix marrow, dragon liver, and other meats are tasted; the
peaches and the wine bestow immortality. Music from invisible
instruments is heard, songs that are not from mortal lips; and the
dances of the visible damsels are the manifestations of the joy of
eternity in time. 128

The tea ceremonies of Japan are conceived in the spirit of the Taoist
earthly paradise. The tearoom, called "the abode of fancy," is an
ephemeral structure built to enclose a moment of poetic intuition.
Called too "the abode of vacancy," it is devoid of ornamentation.
Temporarily it contains a single picture or flower-arrangement. The
teahouse is called "the abode of the unsymmetrical": the unsymmetrical
suggests movement; the purposely unfinished leaves a vacuum into which
the imagination of the beholder can pour.

The guest approaches by the garden path, and must stoop through the low
entrance. He makes obeisance to the picture or flower-arrangement, to
the singing kettle, and takes his place on the floor. The simplest
object, framed by the controlled simplicity of the tea house, stands out
in mysterious beauty, its silence holding the secret of temporal
existence. Each guest is permitted to complete the experience in
relation to himself. The members of the company thus contemplate the
universe in miniature, and become aware of their hidden fellowship with
the immortals.

The great tea masters were concerned to make of the divine wonder an
experienced moment; then out of the teahouse the influence was carried
into the home; and out of the home distilled

into the nation. 129 During the long and peaceful Tokugawa period
(1603-1868), before the arrival of Commodore Perry in 1854, the texture
of Japanese life became so imbued with significant formalization that
existence to the slightest detail was a conscious expression of
eternity, the landscape itself a shrine. Similarly, throughout the
Orient, throughout the ancient world, and in the pre-Columbian Americas,
society and nature represented to the mind the inexpressible. "The
plants, rocks, fire, water, all are alive. They watch us and see our
needs. They see when we have nothing to protect us," declared an old
Apache storyteller, "and it is then that they reveal themselves and
speak to us." 13C This is what the Buddhist calls "the sermon of the
inanimate."

A certain Hindu ascetic who lay down to rest beside the holy Ganges,
placed his feet up on a Shiva-symbol (a "lingam," a combined phallus and
vulva, symbolizing the union of the God with his Spouse). A passing
priest observed the man reposing thus and rebuked him. "How can you dare
to profane this symbol of God by resting your feet on it?" demanded the
priest. The ascetic replied, "Good sir, I am sorry; but will you kindly
take my feet and place them where there is no such sacred lingam?" The
priest seized the ankles of the ascetic and lifted them to the right,
but when he set them down a phallus sprang from the ground and they
rested as before. He moved them again; another phallus received them.
"Ah, I see!" said the priest, humbled; and he made obeisance to the
reposing saint and went his way.

The third wonder of the Bodhisattva myth is that the first wonder
(namely, the bisexual form) is symbolical of the second (the identity of
eternity and time). For in the language of the divine pictures, the
world of time is the great mother womb. The life therein, begotten by
the father, is compounded of her darkness and his light. 131 We are
conceived in her and dwell removed
from the father, but when we pass from the womb of time at death (which
is our birth to eternity) we are given into his hands. The wise realize,
even within this womb, that they have come from and are returning to the
father; while the very wise know that she and he are in substance one.

This is the meaning of those Tibetan images of the union of the Buddhas
and Bodhisattvas with their own feminine aspects that have seemed so
indecent to many Christian critics. According to one of the traditional
ways of looking at these supports of meditation, the female form
(Tibetan: yum) is to be regarded as time and the male (yab) as eternity.
The union of the two is productive of the world, in which all things are
at once temporal and eternal, created in the image of this self-knowing
malefemale God. The initiate, through meditation, is led to the
recollection of this Form of forms ( yab-yum ) within himself. Or on the
other hand, the male figure may be regarded as symbolizing the
initiating principle, the method; in which case the female denotes the
goal to which initiation leads. But this goal is Nirvana (eternity). And
so it is that both the male and the female are to be envisioned,
alternately, as time and eternity. That is to say, the two are the same,
each is both, and the dual form ( yab-yum ) is only an effect of
illusion, which itself, however, is not different from enlightenment.

---

Comparatively, the Hindu goddess Kali (supra, p. 105) is shown
standing on the prostrate form of the god Shiva, her spouse. She
brandishes the sword of death, i.e., spiritual discipline. The
blood-dripping human head tells the devotee that he that loseth his life
for her sake shall find it. The gestures of "fear not" and "bestowing
boons" teach that she protects her children, that the pairs of opposites
of the universal agony are not what they seem, and that for one centered
in eternity the phantasmagoria of temporal "goods" and "evils" is but a
reflex of the mind---as the goddess herself, though apparently trampling
down the god, is actually his blissful dream.

Beneath the goddess of the Island of Jewels (see supra , pp. 103-104)
two aspects of the god are represented: the one, face upward, in union
with her, is the creative, world-enjoying aspect; but the other, turned
away, is the deus absconditus , the divine essence in and by itself,
beyond event and change, inactive, dormant, void, beyond even the wonder
of the hermaphroditic mystery. (See Zimmer, Myths and Symbols in Indian
Art and Civilization , pp. 210-214.)

---

This is a supreme statement of the great paradox by which the wall of
the pairs of opposites is shattered and the candidate admitted to the
vision of the God, who when he created man in his own image created him
male and female. In the male's right hand is held a thunderbolt that is
the counterpart of himself, while in his left he holds a bell,
symbolizing the goddess. The thunderbolt is both the method and
eternity, whereas the bell is "illumined mind"; its note is the
beautiful sound of eternity that is heard by the pure mind throughout
creation, and therefore within itself . 133

Precisely the same bell is rung in the Christian Mass at the moment when
God, through the power of the words of the consecration, descends into
the bread and wine. And the Christian reading of the meaning also is the
same: Et Verbum caro factum est, lu i.e., "The Jewel is in the Lotus":
Om mani padme hum .

## vi. The Ultimate Boon

When the Prince of the Lonesome Island had remained six nights and days
on the golden couch with the sleeping Queen of Tubber Tintye, the couch
resting on wheels of gold and the wheels turning continually---the couch
going round and round, never stopping night or day---on the seventh
morning he said, '"It is time for me now to leave this place.' So he
came down and filled the three bottles with water from the flaming well.
In the golden chamber was a table of gold, and on the table a leg of
mutton with a loaf of bread; and if all the men of Erin were to eat for
a twelvemonth from the table, the mutton and the bread would be in the
same form after the eating as before.

"The Prince sat down, ate his fill of the loaf and the leg of mutton,
and left them as he had found them. Then he rose up, took his three
bottles, put them in his wallet, and was leaving the chamber, when he
said to himself: 'It would be a shame to go away without leaving
something by which the Queen may know who was here while she slept.' So
he wrote a letter, saying that the son of the King of Erin and the Queen
of the Lonesome Island had spent six days and nights in the golden
chamber of Tubber Tintye, had taken away three bottles of water from the
flaming well, and had eaten from the table of gold. Putting his letter
under the pillow of the Queen, he went out, stood in the open window,
sprang on the back of the lean and shaggy little horse, and passed the
trees and the river unharmed." 136

The ease with which the adventure is here accomplished signifies that
the hero is a superior man, a born king. Such ease
distinguishes numerous fairy tales and all legends of the deeds of
incarnate gods. Where the usual hero would face a test, the elect
encounters no delaying obstacle and makes no mistake. The well is the
World Navel, its flaming water the indestructible essence of existence,
the bed going round and round being the World Axis. The sleeping castle
is that ultimate abyss to which the descending consciousness submerges
in dream, where the individual life is on the point of dissolving into
undifferentiated energy: and it would be death to dissolve; yet death,
also, to lack the fire. The motif (derived from an infantile fantasy) of
the inexhaustible dish, symbolizing the perpetual life-giving,
form-building powers of the universal source, is a fairy-tale
counterpart of the mythological image of the cornucopian banquet of the
gods. While the bringing together of the two great symbols of the
meeting with the goddess and the fire theft reveals with simplicity and
clarity the status of the anthropomorphic powers in the realm of myth.
They are not ends in themselves, but guardians, embodiments, or
bestowers, of the liquor, the milk, the food, the fire, the grace, of
indestructible life.

Such imagery can be readily interpreted as primarily, even though
perhaps not ultimately, psychological; for it is possible to observe, in
the earliest phases of the development of the infant, symptoms of a
dawning "mythology" of a state beyond the vicissitudes of time. These
appear as reactions to, and spontaneous defenses against, the
body-destruction fantasies that assail the child when it is deprived of
the mother breast. 137 "The infant reacts with a temper tantrum and the
fantasy that goes with the temper tantrum is to tear everything out of
the mother's body\.... The child then fears retaliation for these
impulses, i.e., that everything will be scooped out of its own inside."
138 Anxieties for the integrity of its body, fantasies of restitution, a
silent, deep requirement for indestructibility and protection against
the
"bad" forces from within and without, begin to direct the shaping
psyche; and these remain as determining factors in the later neurotic,
and even normal, life activities, spiritual efforts, religious beliefs,
and ritual practices of the adult.

The profession, for example, of the medicine man, this nucleus of all
primitive societies, "originates \... on the basis of the infantile
body-destruction fantasies, by means of a series of defence mechanisms."
139 In Australia a basic conception is that the spirits have removed the
intestines of the medicine man and substituted pebbles, quartz crystals,
a quantity of rope, and sometimes also a little snake endowed with
power. 140 "The first formula is abreaction in fantasy (my inside has
already been destroyed) followed by reaction-formation (my inside is not
something corruptible and full of faeces, but incorruptible, full of
quartz crystals). The second is projection: 'It is not I who am trying
to penetrate into the body but foreign sorcerers who shoot
disease-substances into people.' The third formula is restitution: 'I am
not trying to destroy people's insides, I am healing them.' At the same
time, however, the original fantasy element of the valuable
body-contents torn out of the mother returns in the healing technique:
to suck, to pull, to rub something out of the patient." 141

Another image of indestructibility is represented in the folk idea of
the spiritual "double"-an external soul not afflicted by the losses and
injuries of the present body, but existing safely in some place removed.
142 "My death," said a certain ogre, "is far from here and hard to find,
on the wide ocean. In that sea is an island, and on the island there
grows a green oak, and beneath the oak is an iron chest, and in the
chest is a small basket, and in the basket is a hare, and in the hare is
a duck, and in the duck is
an egg; and he who finds the egg and breaks it, kills me at the same
time." 143 Compare the dream of a successful modern businesswoman: "I
was stranded on a desert island. There was a Catholic priest there also.
He had been doing something about putting boards from one island to
another so people could pass. We passed to another island and there
asked a woman where I'd gone. She replied that I was diving with some
divers. Then I went somewhere to the interior of the island where was a
body of beautiful water full of gems and jewels and the other 'I' was
down there in a diving suit. I stood there looking down and watching
myself." 144 There is a charming Hindu tale of a king's daughter who
would marry only the man that found and awakened her double, in the Land
of the Lotus of the Sun, at the bottom of the sea. 145 The initiated
Australian, after his marriage, is conducted by his grandfather to a
sacred cave and there shown a small slab of wood inscribed with
allegorical designs: "This," he is told, "is your body; this and you are
the same. Do not take it to another place or you will feel pain." 146
The Manicheans and the Gnostic Christians of the first centuries a.d.
taught that when the soul of the blessed arrives in heaven it is met by
saints and angels bearing its "vesture of light," which has been
preserved for it.

The supreme boon desired for the Indestructible Body is uninterrupted
residence in the Paradise of the Milk that Never
in their roof garden, are served the inexhaustible, delicious flesh of
the monsters Behemoth, Leviathan, and Ziz, while drinking the liquors of
the four sweet rivers of paradise . 148

It is obvious that the infantile fantasies which we all cherish still in
the unconscious play continually into myth, fairy tale, and the
teachings of the church, as symbols of indestructible being. This is
helpful, for the mind feels at home with the images, and seems to be
remembering something already known. But the circumstance is obstructive
too, for the feelings come to rest in the symbols and resist
passionately every effort to go beyond. The prodigious gulf between
those childishly blissful multitudes who fill the world with piety and
the truly free breaks open at the line where the symbols give way and
are transcended. "O ye," writes Dante, departing from the Terrestrial
Paradise, "O ye who in a little bark, desirous to listen, have followed
behind my craft which singing passes on, turn to see again your shores;
put not out upon the deep; for haply, losing me, ye would remain astray.
The water which I take was never crossed. Minerva breathes, and Apollo
guides me, and nine Muses point out to me the Bears ." 149 Here is the
line beyond which thinking does not go, beyond which all feeling is
truly dead: like the last stop on a mountain railroad from which
climbers step away, and to which they return, there to converse with
those who love mountain air but cannot risk the heights. The ineffable
teaching of the beatitude beyond imagination comes to us clothed,
necessarily, in figures reminiscent of the imagined beatitude of
infancy; hence the deceptive childishness of the tales. Hence, too, the
inadequacy of any merely psychological reading . 150

The sophistication of the humor of the infantile imagery, when inflected
in a skillful mythological rendition of metaphysical doctrine, emerges
magnificently in one of the best known of the great myths of the
Oriental world: the Hindu account of the primordial battle between the
titans and the gods for the liquor of immortality. An ancient earth
being, Kashyapa, "The Turtle Man," had married thirteen of the daughters
of a still more ancient demiurgic patriarch, Daksha, "The Lord of
Virtue." Two of these daughters, Diti and Aditi by name, had given birth
respectively to the titans and the gods. In an unending series of family
battles, however, many of these sons of Kashyapa were being slain. But
now the high priest of the titans, by great austerities and meditations,
gained the favor of Shiva, Lord of the Universe. Shiva bestowed on him a
charm to revive the dead. This gave to the titans an advantage which the
gods, in the next battle, were quick to perceive. They retired in
confusion to consult together, and addressed themselves to the high
divinities Brahma and Vishnu . 151 They were advised to conclude with
their brother-enemies a temporary truce, during which the titans should
be induced to help them churn the Milky Ocean of immortal life for its
butter-Amrita, (a, not, mpita, mortal) "the nectar of deathlessness."
Flattered by the invitation, which they regarded as an admission of
their superiority, the titans were delighted to participate; and so the
epochal co-operative adventure at the beginning of the four ages of the
world cycle began. Mount Mandara was selected as the churning stick.
Vasuki, the King of Serpents, consented to become the churning rope with
which to
twirl it. Vishnu himself, in the form of a tortoise, dove into the Milky
Ocean to support with his back the base of the mountain. The gods laid
hold of one end of the serpent, after it had been wrapped around the
mountain, the titans the other. And the company then churned for a
thousand years.

The first thing to arise from the surface of the sea was a black,
poisonous smoke, called Kalakuta, "Black Summit," namely the highest
concentration of the power of death. "Drink me," said Kalakuta; and the
operation could not proceed until someone should be found capable of
drinking it up. Shiva, sitting aloof and afar, was approached.
Magnificently, he relaxed from his position of deeply indrawn meditation
and proceeded to the scene of the churning of the Milky Ocean. Taking
the tincture of death in a cup, he swallowed it at a gulp, and by his
yoga-power held it in his throat. The throat turned blue. Hence Shiva is
addressed as "Blue Neck," Nilakantha.

The churning now being resumed, presently there began coming up out of
the inexhaustible depths precious forms of concentrated power. Apsarases
(nymphs) appeared, Lakshmi the goddess of fortune, the milk-white horse
named Uchchaihshravas, "Neighing Aloud," the pearl of gems, Kaustubha,
and other objects to the number of thirteen. Last to appear was the
skilled physician of the gods, Dhanvantari, holding in his hand the
moon, the cup of the nectar of life.

Now began immediately a great battle for possession of the invaluable
drink. One of the titans, Rahu, managed to steal a sip, but was beheaded
before the liquor passed his throat; his body decayed but the head
remained immortal. And this head now goes pursuing the moon forever
through the skies, trying again to seize it. When it succeeds, the cup
passes easily through its mouth and out again at its throat: that is why
we have eclipses of the moon.

But Vishnu, concerned lest the gods should lose the advantage,
transformed himself into a beautiful dancing damsel. And while the
titans, who were lusty fellows, stood spellbound by the girl's charm,
she took up the moon-cup of Amrita, teased them with it for a moment,
and then suddenly passed it over to the
gods. Vishnu immediately again transformed himself into a mighty hero,
joined the gods against the titans, and helped drive away the enemy to
the crags and dark canyons of the world beneath. The gods now dine on
the Amrita forever, in their beautiful palaces on the summit of the
central mountain of the world, Mount Sumeru. 152

Humor is the touchstone of the truly mythological as distinct from the
more literal-minded and sentimental theological mood. The gods as icons
are not ends in themselves. Their entertaining myths transport the mind
and spirit, not up to , but past them, into the yonder void; from which
perspective the more heavily freighted theological dogmas then appear to
have been only pedagogical lures: their function, to cart the unadroit
intellect away from its concrete clutter of facts and events to a
comparatively rarefied zone, where, as a final boon, all
existence---whether heavenly, earthly, or infernal---may at last be seen
transmuted into the semblance of a lightly passing, recurrent, mere
childhood dream of bliss and fright. "From one point of view all those
divinities exist," a Tibetan lama recently replied to the question of an
understanding Occidental visitor, "from another they are not real." 153
This is the orthodox teaching of the ancient Tantras: "Ail of these
visualized deities are but symbols representing the various things that
occur on the Path"; 154 as well as a doctrine of the contemporary
psychoanalytical schools. 155 And the same
meta-theological insight seems to be what is suggested in Dante's final
verses, where the illuminated voyager at last is able to lift his
courageous eyes beyond the beatific vision of Father, Son, and Holy
Ghost, to the one Eternal Light. 156

The gods and goddesses then are to be understood as embodiments and
custodians of the elixir of Imperishable Being but not themselves the
Ultimate in its primary state. What the hero seeks through his
intercourse with them is therefore not finally themselves, but their
grace, i.e., the power of their sustaining substance. This miraculous
energy-substance and this alone is the Imperishable; the names and forms
of the deities who everywhere embody, dispense, and represent it come
and go. This is the miraculous energy of the thunderbolts of Zeus,
Yahweh, and the Supreme Buddha, the fertility of the rain of Viracocha,
the virtue announced by the bell rung in the Mass at the consecration,
157 and the light of the ultimate illumination of the saint and sage.
Its guardians dare release it only to the duly proven.

But the gods may be oversevere, overcautious, in which case the hero
must trick them of their treasure. Such was the problem of Prometheus.
When in this mood even the highest gods appear as malignant,
life-hoarding ogres, and the hero who deceives, slays, or appeases them
is honored as the savior of the world.

Maui of Polynesia went against Mahu-ika, the guardian of fire, to wring
from him his treasure and transport it back to mankind. Maui went
straight up to the giant Mahu-ika and said to him: "Clear away the brush
from this level field of ours so that we may contend together in
friendly rivalry." Maui, it must be told, was a great hero and a master
of devices.

"Mahu-ika inquired, 'What feat of mutual prowess and emulation shall it
be?'

"There are perhaps many," writes Dr. J. C. Fliigel, "who would still
retain the notion of a quasi-anthropomorphic Father-God as an
extra-mental reality, even though the purely mental origin of such a God
has become apparent" (The Psychoanalytic Study of the Family , p. 236).

" 'The feat of tossing,' Maui replied.

"To this Mahu-ika agreed; then Maui asked, 'Who shall begin?'

"Mahu-ika answered, 'I shall.'

"Maui signified his consent, so Mahu-ika took hold of Maui and tossed
him up in the air; he rose high above and fell right down into
Mahu-ika's hands; again Mahu-ika tossed Maui up, chanting: 'Tossing,
tossing---up you go!'

"Up went Maui, and then Mahu-ika chanted this incantation:

'Up you go Up you go Up you go Up you go Up you go Up you go Up you go
Up you go Up you go Up you go

to the first level, to the second level, to the third level, to the
fourth level, to the fifth level, to the sixth level, to the seventh
level, to the eighth level, to the ninth level, to the tenth level!'

"Maui turned over and over in the air and started to come down again,
and he fell right beside Mahu-ika; then Maui said, 'You're having all
the fun!'

"'Why indeed!" Mahu-ika exclaimed. 'Do you imagine you can send a whale
flying up into the air?'

"'I can try!' Maui answered.

"So Maui took hold of Mahu-ika and tossed him up, chanting: 'Tossing,
tossing---up you go!'

"Up flew Mahu-ika, and now Maui chanted this spell:

'Up you go to the first level,

Up you go to the second level, Up you go to the third level, Up you go
to the fourth level, Up you go to the fifth level,

Up you go to the sixth level. Up you go to the seventh level,

Up you go to the eighth level.

Up you go to the ninth level.

Up you go---way up in the air!'

"Mahu-ika turned over and over in the air and commenced to fall back;
and when he had nearly reached the ground Maui called out these magic
words: 'That man up there---may he fall right on his head!'

"Mahu-ika fell down; his neck was completely telescoped together, and so
Mahu-ika died." At once the hero Maui took hold of the giant Mahu-ika's
head and cut it off, then he possessed himself of the treasure of the
flame, which he bestowed upon the world. 158

The greatest tale of the elixir quest in the Mesopotamian, preBiblical
tradition is that of Gilgamesh, a legendary king of the Sumerian city of
Erech, who set forth to attain the watercress of immortality, the plant
"Never Grow Old." After he had passed safely the lions that guard the
foothills and the scorpion men who watch the heaven-supporting
mountains, he came, amidst the mountains, to a paradise garden of
flowers, fruits, and precious stones. Pressing on, he arrived at the sea
that surrounds the world. In a cave beside the waters dwelt a
manifestation of the Goddess Ishtar, Siduri-Sabitu, and this woman,
closely veiled, closed the gates against him. But when he told her his
tale, she admitted him to her presence and advised him not to pursue his
quest, but to learn and be content with the mortal joys of life:

Gilgamesh, why dost thou run about this way?

The life that thou art seeking, thou wilt never find.

When the gods created man, they put death upon mankind, and held life in
their own hands.

Fill thy belly, Gilgamesh; day and night enjoy thyself; prepare each day
some pleasant occasion.

Day and night be frolicsome and gay; let thy clothes be handsome , thy
head shampooed, thy body bathed.

Regard the little one who takes thy hand.

Let thy wife be happy against thy bosom } 59

But when Gilgamesh persisted, Siduri-Sabitu gave him permission to pass
and apprised him of the dangers of the way.

The woman instructed him to seek the ferryman Ursanapi, whom he found
chopping wood in the forest and guarded by a group of attendants.
Gilgamesh shattered these attendants (they were called "those who
rejoice to live," "those of stone") and the ferryman consented to convey
him across the waters of death. It was a voyage of one and one-half
months. The passenger was warned not to touch the waters.

Now the far land that they were approaching was the residence of
Utnapishtim, the hero of the primordial deluge , 160 here abiding with
his wife in immortal peace. From afar Utnapishtim spied the approaching
little craft alone on the endless waters, and he wondered in his heart:

Why are "those of stone ,y of the boat shattered,

And someone who is not of my service sailing in the boat?

That one who is coming: is he not a man?

Gilgamesh, on landing, had to listen to the patriarch's long recitation
of the story of the deluge. Then Utnapishtim bid his visitor sleep, and
he slept for six days. Utnapishtim had his wife
bake seven loaves and place them by the head of Gilgamesh as he lay
asleep beside the boat. And Utnapishtim touched Gilgamesh, and he awoke,
and the host ordered the ferryman Ursanapi to give the guest a bath in a
certain pool and then fresh garments. Following that, Utnapishtim
announced to Gilgamesh the secret of the plant.

Gilgamesh, something secret I will disclose to thee, and give thee thine
instruction:

That plant is like a brier in the field;

its thorn, like that of the rose, will pierce thy hand.

But if thy hand attain to that plant, thou wilt return to thy native
land.

The plant was growing at the bottom of the cosmic sea.

Ursanapi ferried the hero out again into the waters. Gilgamesh tied
stones to his feet and plunged . 161 Down he rushed, beyond every bound
of endurance, while the ferryman remained in the boat. And when the
diver had reached the bottom of the bottomless sea, he plucked the
plant, though it mutilated his hand, cut off the stones, and made again
for the surface. When he broke the surface and the ferryman had hauled
him back into the boat, he announced in triumph:

Ursanapi, this plant is the one . ..

By which Man may attain to full vigor.

I will bring it back to Erech of the sheep-pens.. . .

Its name is: "In his age, Man becomes young again. "

I will eat of it and return to the condition of my youth.

They proceeded across the sea. When they had landed, Gilgamesh bathed in
a cool water-hole and lay down to rest. But
while he slept, a serpent smelled the wonderful perfume of the plant,
darted forth, and carried it away. Eating it, the snake immediately
gained the power of sloughing its skin, and so renewed its youth. But
Gilgamesh, when he awoke, sat down and wept, "and the tears ran down the
wall of his nose." 162

To this very day, the possibility of physical immortality charms the
heart of man. The Utopian play by Bernard Shaw, Back to Methuselah ,
produced in 1921, converted the theme into a modem socio-biological
parable. Four hundred years earlier the more literal-minded Juan Ponce
de Leon discovered Florida in a search for the land of "Bimini," where
he had expected to find the fountain of youth. While centuries before
and far away, the Chinese philosopher Ko Hung spent the latter years of
a long lifetime preparing pills of immortality. "Take three pounds of
genuine cinnabar," Ko Hung wrote, "and one pound of white honey. Mix
them. Dry the mixture in the sun. Then roast it over a fire until it can
be shaped into pills. Take ten pills the size of a hemp seed every
morning. Inside of a year, white hair will turn black, decayed teeth
will grow again, and the body will become sleek and glistening. If an
old man takes this medicine for a long period of time, he will develop
into a young man. The one who takes it constantly will enjoy eternal
life, and will not die." 163 A friend one day arrived to pay a visit to
the solitary experimenter and philosopher, but all he found were Ko
Hung's empty clothes.

m 2 •pj le ajjQvg rendering is based on P. Jensen,
Assyrisch-babylonische Mythen und Epen (Keilinschriftliche Bibliothek,
VI, I; Berlin, 1900), pp. 116-273. The verses quoted appear on pp. 223,
251, 251-253. Jensen's version is a line-for-line translation of the
principal extant text, an Assyrian version from King Ashurbanipal's
library (668-626 b.c.). Fragments of the very much older Babylonian
version (see supra , p. 175) and still more ancient Sumerian original
(third millennium B.c.) have also been discovered and deciphered.

The old man was gone; he had passed into the realm of the immortals. 164

The research for physical immortality proceeds from a misunderstanding
of the traditional teaching. On the contrary, the basic problem is: to
enlarge the pupil of the eye, so that the body with its attendant
personality will no longer obstruct the view. Immortality is then
experienced as a present fact: "It is here! It is here!" 165

"All things are in process, rising and returning. Plants come to
blossom, but only to return to the root. Returning to the root is like
seeking tranquility. Seeking tranquility is like moving toward destiny.
To move toward destiny is like eternity. To know eternity is
enlightenment, and not to recognize eternity brings disorder and evil.

"Knowing eternity makes one comprehensive; comprehension makes one
broadminded; breadth of vision brings nobility; nobility is like heaven.

"The heavenly is like Tao. Tao is the Eternal. The decay of the body is
not to be feared." 166

The Japanese have a proverb: "The gods only laugh when men pray to them
for wealth." The boon bestowed on the worshiper is always scaled to his
stature and to the nature of his dominant desire: the boon is simply a
symbol of life energy stepped down to the requirements of a certain
specific case. The irony, of course, lies in the fact that, whereas the
hero who has won the favor of the god may beg for the boon of perfect
illumination, what he generally seeks are longer years to live, weapons
with which to slay his neighbor, or the health of his child.

The Greeks tell of King Midas, who had the luck to win from Bacchus the
offer of whatsoever boon he might desire. He asked that everything he
touched should be turned to gold. When he went his way, he plucked,
experimentally, the twig of an oak tree
and it was immediately gold; he took up a stone, it had turned to gold;
an apple was a golden nugget in his hand. Ecstatic, he ordered prepared
a magnificent feast to celebrate the miracle. But when he sat down and
set his fingers to the roast, it was transmuted; at his lips the wine
became liquid gold. And when his little daughter, whom he loved beyond
anything on earth, came to console him in his misery, she became, the
moment he embraced her, a pretty golden statue.

The agony of breaking through personal limitations is the agony of
spiritual growth. Art, literature, myth and cult, philosophy, and
ascetic disciplines are instruments to help the individual past his
limiting horizons into spheres of ever-expanding realization. As he
crosses threshold after threshold, conquering dragon after dragon, the
stature of the divinity that he summons to his highest wish increases,
until it subsumes the cosmos. Finally, the mind breaks the bounding
sphere of the cosmos to a realization transcending all experiences of
form---all symbolizations, all divinities: a realization of the
ineluctable void.

So it is that when Dante had taken the last step in his spiritual
adventure, and came before the ultimate symbolic vision of the Triune
God in the Celestial Rose, he had still one more illumination to
experience, even beyond the forms of the Father, Son, and Holy Ghost.
"Bernard," he writes, "made a sign to me, and smiled, that I should look
upward; but I was already, of myself, such as he wished; for my sight,
becoming pure, was entering more and more, through the radiance of the
lofty Light which in Itself is true. Thenceforward my vision was greater
than our speech, which yields to such a sight, and the memory yields to
such excess." 167

"There goes neither the eye, nor speech, nor the mind: we know It not;
nor do we see how to teach one about It. Different It is from all that
are known, and It is beyond the unknown as well." 168

This is the highest and ultimate crucifixion, not only of the hero, but
of his god as well. Here the Son and the Father alike are
annihilated---as personality-masks over the unnamed. For just as the
figments of a dream derive from the life energy of one dreamer,
representing only fluid splittings and complications of that single
force, so do all the forms of all the worlds, whether terrestrial or
divine, reflect the universal force of a single inscrutable mystery: the
power that constructs the atom and controls the orbits of the stars.

That font of life is the core of the individual, and within himself he
will find it---if he can tear the coverings away. The pagan Germanic
divinity Othin (Wotan) gave an eye to split the veil of light into the
knowledge of this infinite dark, and then underwent for it the passion
of a crucifixion:

I ween that I hung on the windy tree,

Hung there for nights full nine;

With the spear I was wounded, and offered I was To Othin, myself to
myself,

On the tree that none may ever know What root beneath it runs. 169

The Buddha's victory beneath the Bo Tree is the classic Oriental example
of this deed. With the sword of his mind he pierced the bubble of the
universe---and it shattered into nought. The whole world of natural
experience, as well as the continents, heavens, and hells of traditional
religious belief, exploded--- together with their gods and demons. But
the miracle of miracles was that though all exploded, all was
nevertheless thereby renewed, revivified, and made glorious with the
effulgence of true being. Indeed, the gods of the redeemed heavens
raised their voices in harmonious acclaim of the man-hero who had
penetrated beyond them to the void that was their life and source:
"Flags and banners erected on the eastern rim of the world let their
streamers fly to the western rim of the world; likewise
those erected on the western rim of the world, to the eastern rim of the
world; those erected on the northern rim of the world, to the southern
rim of the world; and those erected on the southern rim of the world, to
the northern rim of the world; while those erected on the level of the
earth let theirs fly until they beat against the Brahma-world; and those
of the Brahma-world let theirs hang down to the level of the earth.
Throughout the ten thousand worlds the flowering trees bloomed; the
fruit trees were weighted down by the burden of their fruit;
trunk-lotuses bloomed on the trunks of trees; branch-lotuses on the
branches of trees; vine-lotuses on the vines; hanging-lotuses in the
sky; and stalk-lotuses burst through the rocks and came up by sevens.
The system of ten thousand worlds was like a bouquet of flowers sent
whirling through the air, or like a thick carpet of flowers; in the
intermundane spaces the eight-thousand-leaguelong hells, which not even
the light of seven suns had formerly been able to illumine, were now
flooded with radiance; the eighty-four-thousand-league-deep ocean became
sweet to the taste; the rivers checked their flowing; the blind from
birth received their sight; the deaf from birth their hearing; the
crippled from birth the use of their limbs; and the bonds and fetters of
captives broke and fell off." 170
