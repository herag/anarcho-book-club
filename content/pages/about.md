Title: About
Subtitle: Anarchy, Free Software, What Licensing, No Compromise
Date: 2021-03-05
save_as: pages/about.html
header_cover: images/books.jpg

## Anarchy

Anarchy is the term that this book club uses to describe that which is inherently human, that which is not ruled over or held down by an oppressive power. Anarchy is a state of being, not a way of life or a religion or a political party. Anarchy is choosing to say, *I am human, I am an individual*. There are many different creeds and manifestos proclaiming anarchy in the name of one some political movement, but the Anarcho Book Club recognized that anarchy starts in the mind of the individual and spreads outward to affect the lives of those around us, asserting that no power can assume dominance over us.

## Free Software

The goal of anarchism is pure and unadulterated freedom. Part of the issue of freedom is that so many claim that they are free, but voluntarily chain themselves to devices that are not free. Anarchists must realize that freedom means that we must be free to exist without oppression.

Oppression, in the technical sense, is the oppression of massive companies and overreaching governments. The only way to secure such freedom is through free and open source software. This site and its administrator urges any listeners/readers to strive to use only free and open source software in their daily lives. 

To that effect we will soon have pages up for the best free software out there that we can find as a separate part of the site.

## What Licensing

Until recently we used the WTFPL at Anarcho Book Club. However, it has come to my attention that there is a license that is a little more appropriate for the type of content that the book club produces. So we will be using the [Creative Commons Zero License](https://creativecommons.org/publicdomain/zero/1.0/legalcode) retroactively on all works and creative expressions produced on this site. Any contributor to the site will do so under the Creative Commons License, which means all code, artistic endeavors, and anything else produced by the Anarcho Book Club is in the Public Domain. No rights reserved because we don't have the right to impose the use of force on anyone in order to gain some sort of monopoly on the thoughts and ideas shared here. 

With that said, many of the ebooks and other works used by this site have their own licenses that are not controlled by the Club. So use them at your own behest. 

## No Compromise

When we speak of anarchist principles, or principles of freedom, we are talking about the things in all of our lives that affect us. No Compromise is the pledge to live free of the rule of the elite, be it in our daily analog lives or in our digital lives. We always look for the path that gives us the most freedom, whether it be by choosing free software, when proprietary software is simply more convenient to our purpose, or choosing to look to an alternative to the mega corps for things like food and daily necessities. We strive every day to live free of compromise in order to become the anarchists that we claim to be.

## In Summary

Freedom is a choice that we all must make. To throw off the chains of the oppressor and fight back in our own way is the utmost of our cause. The armaments provided to us are knowledge and wisdom, which we, at the Anarcho Book Club, seek to mine and eek out of ever morsel of text that we can find. Pair that with the power of Free Software the strength of Human Action, we can accomplish any goal that we set our minds to.

---

If you'd like to donate to Anarcho Book Club, send your Bitcoin to:

bc1qu878eauxjj67c323df8z8smvypkkntmjkmmldf

There is also an official Monero address you can donate to, if you'd like:

43wjjws817qjKuvK7eTmW3jS6jcRzJpCY3i4C5mg8aU3eR2YfnzGJCwiifakaQgPQQHHQGBpvhA3bFTAY2Q47X8y1KeUYyw

Thank you! 
