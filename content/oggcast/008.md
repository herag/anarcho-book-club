Title: The Metamorphosis
Date: 2021-01-23 10:42
Modified: 2021-03-10 21:40
Category: literature
slug: 008
authors: Herag Anarchivist
summary: In which we discuss the anarchist themes in *The Metamorphosis* by Franz Kafka.
header_cover: images/kafka-cover.jpg
header_avatar: images/kafka.jpg

*The Metamorphosis* was a book written in 1915. It was a fantastic little read that covers the debt of a family that weighs so heavily on the main character of the book, Gregor Samsa, that is causes him to transform into an unsightly creature. I give a little summary, then go into my thoughts and how it relates to anarchism in today's society.

You can find this book on [Anarcho Book Club](https://anarchobook.club) as well as on [Project Gutenberg](http://www.gutenberg.org/ebooks/5200).

Into/Outro music is from Kai Engel's Chapter Four - Fall album. You can get that album and many more of his albums from his Bandcamp [page](https://kaiengel.bandcamp.com/).

<audio controls><source src="oggs/008-metamorphosis.ogg" type="audio/ogg"></audio>
