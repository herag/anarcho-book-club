Title: Library
Subtitle: A classic is something that everybody wants to have read and nobody wants to read. -Mark Twain
Date: 2021-03-23 15:12
save_as: pages/library.html
header_cover: images/library.png



## Bakunin, Michael

[God and the State]({static}/docs/bakunin/god-and-the-state.epub)

[The Capitalist System]({static}/docs/bakunin/the-capitalist-system.epub)

## Campbell, Joseph

[The Hero With a Thousand Faces]({static}/docs/campbell/the-hero-with-a-thousand-faces.epub) [HTML](campbell.html)

## Chomsky, Noam

[On Language]({static}/docs/chomsky/on-language.epub)

[On Anarchism]({static}/docs/chomsky/on-anarchism.epub)

## Goldman, Emma

[Anarchism and Other Essays]({static}/docs/goldman/anarchism.epub)

## Graeber, David

[Debt: The First 5000 Years]({static}/docs/graeber/debt.epub)

## Kafka, Franz

[The Metamorphosis]({static}/docs/kafka/the-metamorphosis.epub)

## Kinsella, Stephen

[Against Intellectual Property]({static}/docs/kinsella/against-intellectual-property.epub)

## Klaatu

You can get it from [SmashWords](https://www.smashwords.com/books/view/582453) or download it here [Computing Without Compromise]({static}/docs/klaatu/computing-without-compromise.epub)

## Konkin, Samuel Edward, III

[Agorist Class Theory]({static}/docs/konkin/agorist-class-theory.epub)

## Kropotkin, Petr

[The Conquest of Bread]({static}/docs/kropotkin/the-conquest-of-bread.epub)

[Mutual Aid]({static}/docs/kropotkin/mutual-aid.epub)

## Vonnegut, Kurt

[Player Piano]({static}/docs/vonnegut/player-piano.epub)

## Whitman, Walt

[Leaves of Grass]({static}/docs/whitman/leaves-of-grass.epub)


