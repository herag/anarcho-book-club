Title: Briar
Subtitle: a review
Date: 2021-04-19
save_as: pages/briar.html

[Briar](https://briarproject.org) is a decentralized P2P federated messaging client. This is the Anarcho Book Club review of this application.

#### Introduction to Briar

First off, I have used quite a few messaging platforms in the past 3-4 years and feel like I have a little experience in this area now. So far, my favorite has been [XMPP](https://xmpp.org), though the quality of chats and features vary between different clients. Now Briar is new on the scene and so far I have only found one client for it. It is an Android client directly from the folks at Briar. Since there is not yet a diversity of clients for Briar, we won't be comparing and criticizing individual clients in this review.

Briar has a number of features that interest me as a private individual.

- encrypted messaging
- traffic routed through [Tor](https://torproject.org)
- group messaging
- simplicity

We will go through each of these.

#### Encrypted Messaging

Now I have not done a very in depth analysis of the encryption that Briar uses, but from their website I am given a little bit of confidence in their encryption when I learned that Michael Rogers, who started Briar, has builts peer-to-peer systems before and they have multiple security consultants working for the project as well. *[source](https://briarproject.org/about-us/)*

So I asked myself, what type of encryption does Briar use? I am not extremely well-versed in the intricacies of encryption protocols, but I can generally understand the English sentences that they use to describe them. It seems to me that Briar does not focus their efforts of explanation so much on what the encryption is, but they do a put a great deal of effort in explaining the model they use to distribute attacks and to reduce the surface area of attack. Again, I will not claim to be an encryption/security expert, but this definitely checks a box in my list for security. Their transparency with their threat model also is very encouraging, enough that I think it is a very good method for securing messages. 

Now, I could be wrong, so feel free to contact me about this.

#### Tor Routing

Tor routing is a very interesting approach for helping to anonymize traffic. While there are [some concerns](https://drewdevault.com/2019/04/19/Your-VPN-is-a-serious-choice.html) with how private the Tor service is, but in the end, it does help to anonymize your data. That combined with the encryption that Briar offers makes for a great combo. I honestly don't see many weak points here.

The one drawback that I've found with using Tor is that my phone's battery life dies off very quickly. I was lucky enough to work where I can charge my phone constantly, but that was not sustainable and gave me serious doubts about Briar as my main messaging service. Those doubt quickly faded away when I switched Tor from using bridges to using Tor network without bridges. I am not a Tor expert, but if it saves my battery, then I'm okay with that. If I understand it right, however, some people will have to rely on bridges to get a good connection through Tor. For me, that does not seem to be the case, so crisis averted!

I have to comment here about how simple Briar is to use with Tor as well. Most Tor enabled applications I have used require some setup and a bit of checking to make sure your traffic is actually being routed through Tor. Briar simplifies this and makes the process basically a background issue. The user never really interacts with Tor settings unless they are like me and like to try to tweak things way too much. In most cases, Briar seems to just work. No complex worries about routing traffic through a Tor client or anything, it is all built in to the Briar client, which really simplifies everything for the user.

#### Group Messaging

I have used a lot of clients before, but one thing I have grown accustomed to is the ability to do group messaging. Briar has a couple of options for this. The first is a simple group message, which is to say an encrypted message between a group of people. This is the feature that I am most accustomed to in other clients. It seems to work pretty well and the threaded view is pretty neat.

However, the group messaging UI/UX leaves you wanting a little more. Sometimes the threads are difficult to keep up with. Sometimes the whole UI is just a bit overwhelming. I am a fan of the functionality, but the UX leaves something to be desired yet. It *feels* beta, to be honest. With that said, I fully expect it to develop very nicely, and there is a really easy way to give your input on features like this. You can either create an issue with their GitLab instance, or you can simply use the built in *Feedback* button in the sidebar of the app. 

The *Feedback* option is one of my favorite things about the Briar app because it lets you send suggestions and questions to the developers very easily and even anonymously if you wish. This is beneficial to both the developers and the users because it creates a continuous stream of feedback for the developers and allows a simple channel of communication between the two parties. 

The other type of group that you can make is called a *Forum*. I have not fully explored this yet, but one thing that makes it different from the group is that anyone in the forum can invite any of their contacts to join. Now, the UI feels exactly the same as a group, but in groups, only the creator of the group can invite members to join. This places a control on who is able to see the messages exchanged within the group, which, in turn, makes it a little more secure than the forum. 

There are a couple of things about the forum option that I would like to see in the future. 

First, it needs to be a little more distinct from the UI perspective. Since a forum looks so similar to a group (in fact, I believe that are exactly the same from a UI perspective), it leaves me feeling a bit uneasy. I'm not entirely sure why, to be completely honest, but I would love to see a more distinct separation in the UI for the forum and the group. This is, however, not a hard requirement because I am very happy that there are these two options, but if I had a wishlist for this client, that would be it.

Second, it would be nice if there were a way to share a link to the forum so that it could be shared publicly with a community and no one would necessarily need an invite to join the forum. This would create a more *agorist* or *democratic* environment for the forum and would help to set it apart from the group.

In the end, I love the group and forum features from Briar, so don't take these points as necessarily a complaint against the developers of Briar; I think of it more as constructive thoughts about these features.

#### Simplicity

Finally we get to the fourth point that I wanted to make about this app. Simplicity. I have used Conversations for XMPP on Android and it works fairly well. I have used Element for Matrix on Android and it... well it is okay. But this is where Briar truly shines. It is truly simple. Not only does it rid us of the need for central servers, but with that, there is no real *sign up* form to speak of. Sure, you pick your username and a password, but you don't have to rely on a third-party to verify those things or to host those details on a server because you effectively *are* your own server! It is fantastic!

In addition to that, the UI is very simple. Nothing overly complex about it at all, but even though it is simple, you still get the feeling of control. There are so many design philosophies that take away user power in favor of simplicity, but the Briar team have made an application that gives you the power to own the code you use and to not be confused by it. It is the happy medium between overly complex and excessively simple. I am very very happy by how they have utilized their design choices to this effect.

#### Conclusion

In the end, I love Briar for many reasons. First and foremost, the freedom from central servers really does it for me. With that said, there is one thing that I want to see from the Briar team more than anything. That is portability of their client. In short, I would love to see a commandline client fully developed and perhaps more than one desktop client developed as well. I know that they currently have a [GTK client developed](https://code.briarproject.org/briar/priar-gtk), but I would like to see more options for clients available. It I knew how to code on that level I would even try my hand at it, but alas I haven't the skills currently to complete the task. So I am left to rely on the community, and perhaps you would be interested in developing a client for Briar? I'm sure it would be fine! The code is open source, so maybe give it a go!

Anyway, my conclusion on this app is that I am very excited by it. I plan to continue to use it in conjunction with my XMPP client as well for now, but maybe not too far in the future I will see you on Briar!

If you want to contact me on Briar, I will be adding my Briar address to Anarcho Book Club's list of official contact information shortly after this post is published. Also, I have posted it on Mastodon as well. But for now, here it is:

briar://aaisip4kh5jsb6opofofoc2qnlqsd4rb2c6te5nlp5w4wmdkvumgeo


