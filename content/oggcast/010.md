Title: Player Piano, part one
Date: 2021-02-05 10:50
Modified: 2021-03-10 21:53
Category: literature
slug: 010
authors: Herag Anarchivist
header_cover: images/player-piano.jpg
header_avatar: images/vonnegut.jpg
summary: In which we discuss the first part of *Player Piano* by Kurt Vonnegut.

Today we discuss the first few chapters of *Player Piano* by Kurt Vonnegut. We explore some of the archetypes setup and explained by Mr Vonnegut. We explore the distopic world in which all machines are automated and how the people of that world, too, may be automated, and how that relates to our every day life today.

[The Hollow Men](docs/other/the-hollow-men.pdf)

[Alexander's Ragtime Band](media/alexanders-ragtime-band.ogg)

Into/Outro music is from Kai Engel's Chapter Four - Fall album. You can get that album and many more of his albums from his Bandcamp [page](https://kaiengel.bandcamp.com/).

<audio controls><source src="oggs/010-player-piano.ogg" type="audio/ogg"></audio>
